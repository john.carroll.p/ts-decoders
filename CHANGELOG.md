# Change Log

This repo attempts to follow [semantic versioning](https://semver.org/).

## Unreleased

## 1.1.0 FEATURE (2024/6/22)

### Features

- Update `objectD` decoder with a new `keepExcessProperties` option.
- Update `Decoder#chain` to allow passing a function which returns another Decoder.

## 0.7.4 FEATURE (2022/2/28)

### Features

- Correct typing of `constantD` decoder to be more specific/accurate.
- Correct typing of `assert` so that promise overload is properly triggered.

## 0.7.3 FEATURE (2019/12/5)

### Features

- Added `removeUndefinedProperties?: boolean` option to `objectD()`

## 0.7.2 FEATURE (2019/12/3)

### Features

- Added `Decoder#catch()` and `AsyncDecoder#catch()`
- Added `Decoder#toAsyncDecoder()`

## 0.7.1 FIX (2019/11/28)

### Fixes

- Fixed typing in `predicateD()` overload

## 0.7.0 BREAKING (2019/11/25)

### Breaking

- Change `DecoderErrorMsgArg` type from `string | ((input: any, errors: DecoderError[]) => DecoderError | DecoderError[])` to `string | ((input: unknown, errors: DecoderError[]) => DecoderError | DecoderError[])`

### Feature

- Improved typing of `predicateD()` with added overload

## 0.6.0 BREAKING (2019/11/7)

### Breaking

- Change `DecoderErrorMsgArg` type from `string | ((errors: DecoderError[]) => DecoderError | DecoderError[])` to `string | ((input: any, errors: DecoderError[]) => DecoderError | DecoderError[])`

## 0.5.0 BREAKING (2019/10/25)

### Breaking

- Remove `Decoder#chain()` type overload which was causing many type inference issues. Not sure if there was actually a problem with the type or if I was running into some kind of typescript bug.

## 0.4.3 FEATURE (2019/10/24)

### Feature

- Improve specificity of `exactlyD` type

## 0.4.2 FIX (2019/10/21)

### Fixes

- Add `AssertDecoderError#name`

## 0.4.1 FIX (2019/10/21)

### Fixes

- Fix construction of `AssertDecoderError`

## 0.4.0 BREAKING (2019/10/21)

### Breaking

- Changed `Decoder#then` to `Decoder#chain` to avoid clashes with `Promise` type

### Feature

- `lazyD` with `promise: true` now accepts an async function which results to a decoder

## 0.3.0 BREAKING (2019/10/21)

### Breaking

- Made `Decoder#decodeFn` readonly
- Made `AsyncDecoder#decodeFn` readonly

### Features

- Add `Decoder#then()` and `AsyncDecoder#then()` which performs a new validation check on `DecoderSuccess`.

## 0.2.0 BREAKING (2019/10/11)

### Breaking

- Remove ability to return errors as a `Map`. Instead, always return errors as an array. `errorMode` option -> `allErrors` option.

## 0.1.0 BREAKING (2019/10/7)

### Breaking

- renamed `optionalD` -> `undefinableD` and `maybeD` -> `optionalD`

## 0.0.4 (2019/10/7)

### Fixes

- [FIX] `stringD` and `predicateD` options interface names

## 0.0.3 (2019/10/6)

- publish and update naming scheme

## 0.0.1 (2019/10/6)

Initial release
