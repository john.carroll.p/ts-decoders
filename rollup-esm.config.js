// rollup.config.js
import typescript from 'rollup-plugin-typescript2';

export default [
  {
    input: './src/index.ts',
    output: {
      file: './build/es2015/main.js',
      name: 'tsDecoders',
      format: 'esm',
    },
    external: [],
    plugins: [
      typescript({
        tsconfig: './tsconfig.esm.json',
      }),
    ],
  },
  {
    input: './src/decoders/index.ts',
    output: {
      file: './build/es2015/decoders.js',
      name: 'tsDecodersDecoders',
      format: 'esm',
    },
    external: ['ts-decoders'],
    plugins: [
      typescript({
        tsconfig: './tsconfig.esm.json',
      }),
    ],
  },
  {
    input: './src/testing/index.ts',
    output: {
      file: './build/es2015/testing.js',
      name: 'tsDecodersTesting',
      format: 'esm',
    },
    external: ['ts-decoders'],
    plugins: [
      typescript({
        tsconfig: './tsconfig.esm.json',
      }),
    ],
  },
  {
    input: './src/testing/jest/index.ts',
    output: {
      file: './build/es2015/jest.js',
      name: 'tsDecodersTestingJest',
      format: 'esm',
    },
    external: ['ts-decoders', 'ts-decoders/testing'],
    plugins: [
      typescript({
        tsconfig: './tsconfig.esm.json',
      }),
    ],
  },
];
