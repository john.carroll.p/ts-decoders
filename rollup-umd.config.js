// rollup.config.js
import typescript from 'rollup-plugin-typescript2';

export default [
  {
    input: './src/index.ts',
    output: {
      file: './build/umd/main.js',
      name: 'tsDecoders',
      format: 'umd',
    },
    external: [],
    plugins: [
      typescript({
        tsconfig: './tsconfig.umd.json',
      }),
    ],
  },
  {
    input: './src/decoders/index.ts',
    output: {
      file: './build/umd/decoders.js',
      name: 'tsDecodersDecoders',
      format: 'umd',
      globals: {
        'ts-decoders': 'tsDecoders',
      },
    },
    external: ['ts-decoders'],
    plugins: [
      typescript({
        tsconfig: './tsconfig.umd.json',
      }),
    ],
  },
  {
    input: './src/testing/index.ts',
    output: {
      file: './build/umd/testing.js',
      name: 'tsDecodersTesting',
      format: 'umd',
      globals: {
        'ts-decoders': 'tsDecoders',
      },
    },
    external: ['ts-decoders'],
    plugins: [
      typescript({
        tsconfig: './tsconfig.umd.json',
      }),
    ],
  },
  {
    input: './src/testing/jest/index.ts',
    output: {
      file: './build/umd/jest.js',
      name: 'tsDecodersTestingJest',
      format: 'umd',
      globals: {
        'ts-decoders': 'tsDecoders',
        'ts-decoders/testing': 'tsDecodersTesting',
      },
    },
    external: ['ts-decoders', 'ts-decoders/testing'],
    plugins: [
      typescript({
        tsconfig: './tsconfig.umd.json',
      }),
    ],
  },
];
