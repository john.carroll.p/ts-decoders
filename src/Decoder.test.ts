import {
  BOOLEAN_ASYNC_DEC,
  BOOLEAN_DEC,
  STRING_ASYNC_DEC,
  STRING_DEC,
} from './testing';

import { AsyncDecoder, Decoder } from './Decoder';
import { DecoderError, DecoderSuccess } from './DecoderResult';

const BOOLEAN_ERROR = new DecoderError(0, 'invalid type', 'must be a boolean');
const TRUE_DECODER = new Decoder((input: boolean) => {
  if (input) return new DecoderSuccess(input);

  return new DecoderError(input, 'invalid-type', 'must be true');
});
const TRUE_ASYNC_DECODER = new AsyncDecoder(async (input: boolean) => {
  if (input) return new DecoderSuccess(input);

  return new DecoderError(input, 'invalid-type', 'must be true');
});

/**
 * Decoder
 */

describe('Decoder', () => {
  it('init', () => {
    expect(STRING_DEC).toBeDecoder();
  });

  it('Decoder#decode', async () => {
    const decoder = BOOLEAN_DEC;

    expect([decoder, true]).toDecodeSuccessfully(new DecoderSuccess(true));
    expect([decoder, false]).toDecodeSuccessfully(new DecoderSuccess(false));
    await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );
    await expect([decoder, Promise.resolve(false)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );

    expect([decoder, 0]).toDecodeWithErrors(BOOLEAN_ERROR);

    await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
      BOOLEAN_ERROR,
    );
  });

  it('Decoder#catch', () => {
    const decoder = BOOLEAN_DEC.catch(input => {
      if (typeof input === 'string') {
        return new DecoderError(input, 'new-error', 'cannot be a string');
      }

      return new DecoderSuccess(false);
    });

    expect([decoder, true]).toDecodeSuccessfully(new DecoderSuccess(true));
    expect([decoder, false]).toDecodeSuccessfully(new DecoderSuccess(false));

    expect([decoder, 'false']).toDecodeWithErrors([
      new DecoderError('false', 'new-error', 'cannot be a string'),
    ]);

    expect([decoder, {}]).toDecodeSuccessfully(new DecoderSuccess(false));
    expect([decoder, 0]).toDecodeSuccessfully(new DecoderSuccess(false));
  });

  it('Decoder#map', async () => {
    const decoder = BOOLEAN_DEC.map((value): boolean => !value);

    expect([decoder, true]).toDecodeSuccessfully(new DecoderSuccess(false));
    expect([decoder, false]).toDecodeSuccessfully(new DecoderSuccess(true));
    await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );
    await expect([decoder, Promise.resolve(false)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );

    expect([decoder, 0]).toDecodeWithErrors(BOOLEAN_ERROR);

    await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
      BOOLEAN_ERROR,
    );
  });

  it('Decoder#toAsyncDecoder', async () => {
    const decoder = BOOLEAN_DEC.toAsyncDecoder();

    expect(decoder).toBeAsyncDecoder();

    await expect([decoder, true]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );
    await expect([decoder, false]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );
    await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );
    await expect([decoder, Promise.resolve(false)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );

    await expect([decoder, 0]).toAsyncDecodeWithErrors(BOOLEAN_ERROR);

    await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
      BOOLEAN_ERROR,
    );
  });

  describe('Decoder#chain', () => {
    it.each([
      [
        'function returning DecoderResult',
        (input: any) => {
          if (input) return new DecoderSuccess(input);

          return new DecoderError(input, 'invalid-type', 'must be true');
        },
      ],
      [
        'function returning Decoder',
        (input: any) => {
          if (input) return new Decoder(input => new DecoderSuccess(input));

          return new Decoder(
            input => new DecoderError(input, 'invalid-type', 'must be true'),
          );
        },
      ],
      ['Decoder', TRUE_DECODER],
    ])('%s', async (_, thenArg) => {
      const decoder = BOOLEAN_DEC.chain(thenArg);

      expect([decoder, true]).toDecodeSuccessfully(new DecoderSuccess(true));
      expect([decoder, false]).toDecodeWithErrors([
        new DecoderError(false, 'invalid-type', 'must be true'),
      ]);
      await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(true),
      );
      await expect([decoder, Promise.resolve(false)]).toAsyncDecodeWithErrors([
        new DecoderError(false, 'invalid-type', 'must be true'),
      ]);

      expect([decoder, 0]).toDecodeWithErrors(BOOLEAN_ERROR);

      await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
        BOOLEAN_ERROR,
      );
    });
  });
});

/**
 * AsyncDecoder
 */

describe('AsyncDecoder', () => {
  it('init', () => {
    expect(STRING_ASYNC_DEC).toBeAsyncDecoder();
  });

  it('AsyncDecoder#decode', async () => {
    const decoder = BOOLEAN_ASYNC_DEC;

    await expect([decoder, true]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );
    await expect([decoder, false]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );
    await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );
    await expect([decoder, Promise.resolve(false)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );

    await expect([decoder, 0]).toAsyncDecodeWithErrors(BOOLEAN_ERROR);

    await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
      BOOLEAN_ERROR,
    );
  });

  it('AsyncDecoder#map', async () => {
    const decoder = BOOLEAN_ASYNC_DEC.map((value): boolean => !value);

    await expect([decoder, true]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );
    await expect([decoder, false]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );
    await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(false),
    );
    await expect([decoder, Promise.resolve(false)]).toAsyncDecodeSuccessfully(
      new DecoderSuccess(true),
    );

    await expect([decoder, 0]).toAsyncDecodeWithErrors(BOOLEAN_ERROR);

    await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
      BOOLEAN_ERROR,
    );
  });

  describe('AsyncDecoder#chain', () => {
    it.each([
      [
        'function',
        (input: any) => {
          if (input) return Promise.resolve(new DecoderSuccess(input));

          return new DecoderError(input, 'invalid-type', 'must be true');
        },
      ],
      ['Decoder', TRUE_DECODER],
      ['AsyncDecoder', TRUE_ASYNC_DECODER],
    ])('%s', async (_, thenArg) => {
      const decoder = BOOLEAN_ASYNC_DEC.chain(thenArg);

      await expect([decoder, true]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(true),
      );
      await expect([decoder, false]).toAsyncDecodeWithErrors([
        new DecoderError(false, 'invalid-type', 'must be true'),
      ]);
      await expect([decoder, Promise.resolve(true)]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(true),
      );
      await expect([decoder, Promise.resolve(false)]).toAsyncDecodeWithErrors([
        new DecoderError(false, 'invalid-type', 'must be true'),
      ]);

      await expect([decoder, 0]).toAsyncDecodeWithErrors(BOOLEAN_ERROR);

      await expect([decoder, Promise.resolve(0)]).toAsyncDecodeWithErrors(
        BOOLEAN_ERROR,
      );
    });
  });

  describe('AsyncDecoder#catch', () => {
    it('sync fn', async () => {
      const decoder = BOOLEAN_ASYNC_DEC.catch(input => {
        if (typeof input === 'string') {
          return new DecoderError(input, 'new-error', 'cannot be a string');
        }

        return new DecoderSuccess(false);
      });

      await expect([decoder, true]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(true),
      );
      await expect([decoder, false]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(false),
      );

      await expect([decoder, 'false']).toAsyncDecodeWithErrors([
        new DecoderError('false', 'new-error', 'cannot be a string'),
      ]);

      await expect([decoder, {}]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(false),
      );
      await expect([decoder, 0]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(false),
      );
    });

    it('async fn', async () => {
      const decoder = BOOLEAN_ASYNC_DEC.catch(async input => {
        if (typeof input === 'string') {
          return new DecoderError(input, 'new-error', 'cannot be a string');
        }

        return new DecoderSuccess(false);
      });

      await expect([decoder, true]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(true),
      );
      await expect([decoder, false]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(false),
      );

      await expect([decoder, 'false']).toAsyncDecodeWithErrors([
        new DecoderError('false', 'new-error', 'cannot be a string'),
      ]);

      await expect([decoder, {}]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(false),
      );
      await expect([decoder, 0]).toAsyncDecodeSuccessfully(
        new DecoderSuccess(false),
      );
    });
  });
});
