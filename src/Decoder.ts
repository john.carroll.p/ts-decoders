import {
  areDecoderErrors,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
} from './DecoderResult';

function coerceDecodeFnResult<T>(result: DecodeFnResult<T>): DecoderResult<T> {
  return result instanceof DecoderError ? [result] : result;
}

export type DecodeFnResult<T> =
  | DecoderSuccess<T>
  | DecoderError
  | DecoderError[];

/**
 * An object which can be used to validate and process `unknown` values
 * and cast them to the appropriate typescript type.
 *
 * @param decodeFn the function which is used to decode input values
 */
export class Decoder<R, I = any> {
  constructor(readonly decodeFn: (input: I) => DecodeFnResult<R>) {}

  decode(input: Promise<I>): Promise<DecoderResult<R>>;
  decode(input: I): DecoderResult<R>;
  decode(input: I | Promise<I>): DecoderResult<R> | Promise<DecoderResult<R>> {
    if (input instanceof Promise) {
      return input.then(
        (res): DecoderResult<R> => coerceDecodeFnResult(this.decodeFn(res)),
      );
    }

    return coerceDecodeFnResult(this.decodeFn(input));
  }

  /**
   * On decode failure, handle the DecoderErrors.
   */
  catch<K>(fn: (input: unknown, errors: DecoderError[]) => DecodeFnResult<K>) {
    return new Decoder<K | R, I>((input: I) => {
      const result = coerceDecodeFnResult(this.decodeFn(input));

      if (areDecoderErrors(result)) {
        return fn(input, result);
      }

      return result;
    });
  }

  /**
   * On decode success, transform a value using the provided
   * transformation function.
   */
  map<K>(fn: (input: R) => K): Decoder<K, I> {
    return new Decoder((input: I) => {
      const result = this.decodeFn(input);

      if (result instanceof DecoderSuccess) {
        return new DecoderSuccess(fn(result.value));
      }

      return result;
    });
  }

  /**
   * On decode success, perform a new validation check.
   */
  chain<K>(fn: (input: R) => DecodeFnResult<K> | Decoder<K, R>): Decoder<K, I>;
  // For some reason, the AsyncDecoder overload causes *many*
  // type inference problems and it just isn't worth it.
  // AsyncDecoder overload must come before Decoder overload
  // chain<K>(decoder: AsyncDecoder<K, R>): AsyncDecoder<K, I>;
  chain<K>(decoder: Decoder<K, R>): Decoder<K, I>;
  chain<K>(
    a: ((input: R) => DecodeFnResult<K> | Decoder<K, R>) | Decoder<K, R>,
  ): Decoder<K, I> {
    const fn = a instanceof Decoder ? a.decodeFn : a;

    return new Decoder((input: I) => {
      const resultA = this.decodeFn(input);

      if (!(resultA instanceof DecoderSuccess)) return resultA;

      const resultB = fn(resultA.value);

      if (resultB instanceof Decoder) {
        return resultB.decode(resultA.value);
      }

      return resultB;
    });
  }

  toAsyncDecoder() {
    const fn = this.decodeFn;

    return new AsyncDecoder<R, I>(async input => fn(input));
  }
}

/**
 * An object which can be used to validate and process `unknown` values
 * and cast them to the appropriate typescript type. Unlike `Decoder`,
 * `AsyncDecoder` can receive a `decodeFn` which returns a promise.
 *
 * @param decodeFn the function which is used to decode input values
 */
export class AsyncDecoder<R, I = any> {
  constructor(readonly decodeFn: (input: I) => Promise<DecodeFnResult<R>>) {}

  async decode(input: I | Promise<I>): Promise<DecoderResult<R>> {
    return coerceDecodeFnResult(await this.decodeFn(await input));
  }

  /**
   * On decode failure, handle the DecoderErrors.
   */
  catch<K>(
    fn: (
      input: unknown,
      errors: DecoderError[],
    ) => DecodeFnResult<K> | Promise<DecodeFnResult<K>>,
  ) {
    return new AsyncDecoder<K | R, I>(async (input: I) => {
      const result = coerceDecodeFnResult(await this.decodeFn(input));

      if (areDecoderErrors(result)) {
        return fn(input, result);
      }

      return result;
    });
  }

  /**
   * On decode success, transform a value using the provided
   * transformation function.
   */
  map<K>(fn: (input: R) => K | Promise<K>): AsyncDecoder<K, I> {
    return new AsyncDecoder(async (input: I) => {
      const result = await this.decodeFn(input);

      if (result instanceof DecoderSuccess) {
        return new DecoderSuccess(await fn(result.value));
      }

      return result;
    });
  }

  /**
   * On decode success, perform a new validation check.
   */
  chain<K>(
    fn: (input: R) => DecodeFnResult<K> | Promise<DecodeFnResult<K>>,
  ): AsyncDecoder<K, I>;
  chain<K>(decoder: Decoder<K, R> | AsyncDecoder<K, R>): AsyncDecoder<K, I>;
  chain<K>(
    a:
      | ((input: R) => DecodeFnResult<K> | Promise<DecodeFnResult<K>>)
      | Decoder<K, R>
      | AsyncDecoder<K, R>,
  ): AsyncDecoder<K, I> {
    const fn =
      a instanceof Decoder || a instanceof AsyncDecoder ? a.decodeFn : a;

    return new AsyncDecoder(async (input: I) => {
      const result = await this.decodeFn(input);

      if (result instanceof DecoderSuccess) {
        return fn(result.value);
      }

      return result;
    });
  }
}

// prettier makes this hard to read
// prettier-ignore
export type DecoderReturnType<T> = T extends Decoder<infer R> ? R
  : T extends AsyncDecoder<infer R> ? R
  : unknown;

// prettier makes this hard to read
// prettier-ignore
export type DecoderInputType<T> = T extends Decoder<any, infer I> ? I
  : T extends AsyncDecoder<any, infer I> ? I
  : unknown;
