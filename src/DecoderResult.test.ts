import {
  areDecoderErrors,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
  isDecoderSuccess,
} from './DecoderResult';

it('new DecoderSuccess', () => {
  const success = new DecoderSuccess(true);
  expect(success).toBeInstanceOf(DecoderSuccess);
  expect(success).toEqual({ value: true });
});

it('new DecoderError', () => {
  const error = new DecoderError(0, 'error', 'error', {
    key: 0,
    location: 'string',
    child: new DecoderError(1, 'child', 'child'),
  });

  expect(error).toBeInstanceOf(DecoderError);
  expect(error).toEqual({
    input: 0,
    type: 'error',
    message: 'error',
    decoderName: '',
    key: 0,
    location: 'string',
    data: undefined,
    child: {
      input: 1,
      type: 'child',
      message: 'child',
      decoderName: '',
      location: '',
      key: undefined,
      child: undefined,
      data: undefined,
    },
  });
});

it('isDecoderSuccess()', () => {
  const success = new DecoderSuccess(null);
  const error = new DecoderError(0, 'message', 'message');

  expect(isDecoderSuccess(success)).toBe(true);
  expect(
    isDecoderSuccess(([success] as unknown) as DecoderResult<unknown>),
  ).toBe(false);
  expect(isDecoderSuccess([error])).toBe(false);
  expect(isDecoderSuccess((error as unknown) as DecoderResult<unknown>)).toBe(
    false,
  );
  expect(
    isDecoderSuccess(('success' as unknown) as DecoderResult<unknown>),
  ).toBe(false);
});

it('areDecoderErrors()', () => {
  const success = new DecoderSuccess(null);
  const error = new DecoderError(0, 'message', 'message');

  expect(areDecoderErrors([error])).toBe(true);
  expect(areDecoderErrors(success)).toBe(false);
  expect(areDecoderErrors((error as unknown) as DecoderResult<unknown>)).toBe(
    false,
  );
  expect(areDecoderErrors(('error' as unknown) as DecoderResult<unknown>)).toBe(
    false,
  );
});
