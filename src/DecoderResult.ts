/**
 * DecoderSuccess returned on a successful call to
 * `Decoder#decode` or `AsyncDecoder#decode`
 */
export class DecoderSuccess<T> {
  constructor(readonly value: T) {}
}

/**
 * DecoderError returned on a failed call to
 * `Decoder#decode` or `AsyncDecoder#decode`
 */
export class DecoderError {
  /** The input that failed validation. */
  input: any;

  /** The type of error. */
  type: string;

  /** A human readable error message. */
  message: string;

  /** The name of the decoder which created this error. */
  decoderName: string;

  /**
   * A human readable string showing the nested location of the error.
   * If the validation error is not nested, location will equal a blank string.
   */
  location: string;

  /** The `DecoderError` which triggered this `DecoderError`, if any */
  child?: DecoderError;

  /**
   * The key associated with this `DecoderError` if any.
   *
   * - example: this could be the index of the array element which
   *   failed validation.
   */
  key?: any;

  /** Convenience property for storing arbitrary data. */
  data: any;

  constructor(
    /** The input that failed validation. */
    input: any,
    /** The type of error. e.g. "invalid type" */
    type: string,
    /** A human readable error message. e.g. "must be a string" */
    message: string,
    options: {
      decoderName?: string;
      location?: string;
      child?: DecoderError;
      key?: any;
      data?: any;
    } = {},
  ) {
    this.input = input;
    this.type = type;
    this.message = message;
    this.decoderName = options.decoderName || '';
    this.location = options.location || '';
    this.child = options.child;
    this.key = options.key;
    this.data = options.data;
  }

  /**
   * Starting with this error, an array of the keys associated with
   * this error as well as all child errors.
   */
  path(): any[] {
    if (this.key === undefined) {
      if (!this.child) return [];

      return this.child.path();
    }

    if (!this.child) return [this.key];

    return [this.key, ...this.child.path()];
  }
}

export type DecoderResult<T> = DecoderSuccess<T> | DecoderError[];

export function isDecoderSuccess<T>(
  result: DecoderResult<T>,
): result is DecoderSuccess<T> {
  return result instanceof DecoderSuccess;
}

export function areDecoderErrors<T>(
  result: DecoderResult<T>,
): result is DecoderError[] {
  return Array.isArray(result);
}
