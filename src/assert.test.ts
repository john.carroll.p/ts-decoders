import { assert, AssertDecoderError } from './assert';
import { BOOLEAN_ASYNC_DEC, BOOLEAN_DEC } from './testing';

/**
 * assert()
 */

describe('assert', () => {
  it('assert()', () => {
    const booleanD = assert(BOOLEAN_DEC);

    expect(booleanD(false)).toBe(false);

    try {
      booleanD(1);
    } catch (e) {
      const err: AssertDecoderError = e;

      expect(err).toBeInstanceOf(AssertDecoderError);
      expect(err.message).toBe('must be a boolean');
      expect(err.errors.length).toBe(1);
      expect(err.errors[0].message).toBe('must be a boolean');
    }

    expect.assertions(5);
  });

  it('async assert()', async () => {
    const booleanD = assert(BOOLEAN_ASYNC_DEC);

    await expect(booleanD(true)).resolves.toBe(true);

    try {
      await booleanD(1);
    } catch (e) {
      const err: AssertDecoderError = e;

      expect(err).toBeInstanceOf(AssertDecoderError);
      expect(err.message).toBe('must be a boolean');
      expect(err.errors.length).toBe(1);
      expect(err.errors[0].message).toBe('must be a boolean');
    }

    expect.assertions(5);
  });
});
