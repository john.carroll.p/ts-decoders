import { DecoderError, DecoderSuccess } from 'ts-decoders';
import { err, errorLocation, ok } from './_util';

/**
 * _util
 */

describe('_util', () => {
  it('ok()', async () => {
    const obj = {};
    expect(ok(obj)).toBeDecoderSuccess(new DecoderSuccess(obj));
    expect(ok(null)).toBeDecoderSuccess(new DecoderSuccess(null));
    expect(ok(undefined)).toBeDecoderSuccess(new DecoderSuccess(undefined));
    expect(ok(10)).toBeDecoderSuccess(new DecoderSuccess(10));
  });

  it('err()', () => {
    let error = err(false, 'null err', 'null err', 'nullDecoder');

    expect(error).toBeDecoderErrors([
      new DecoderError(false, 'null err', 'null err', {
        decoderName: 'nullDecoder',
      }),
    ]);

    error = err(false, 'string err', 'string err', 'STRING_DEC', {
      decoderName: 'customName',
      errorMsg: 'newMessage',
    });

    expect(error).toBeDecoderErrors([
      new DecoderError(false, 'string err', 'newMessage', {
        decoderName: 'customName',
      }),
    ]);

    error = err(false, 'string err', 'string err', 'STRING_DEC', {
      decoderName: 'customName',
      errorMsg: (_, errors) => {
        return [
          ...errors,
          new DecoderError(false, 'null', 'null'),
          new DecoderError(false, 'undefined', 'undefined'),
        ];
      },
    });

    expect(error).toBeDecoderErrors([
      new DecoderError(false, 'string err', 'string err', {
        decoderName: 'customName',
      }),
      new DecoderError(false, 'null', 'null'),
      new DecoderError(false, 'undefined', 'undefined'),
    ]);
  });

  it('errorLocation()', () => {
    expect(errorLocation(1, 'payload[1]')).toBe('[1].payload[1]');
    expect(errorLocation('value', 'payload[1]')).toBe('value.payload[1]');
    expect(errorLocation('value', '[1]')).toBe('value[1]');
    expect(errorLocation('val8ue', '[1]')).toBe('["val8ue"][1]');
    expect(errorLocation('Value', '[1]')).toBe('Value[1]');
    expect(errorLocation('type', 'Value[1]')).toBe('type.Value[1]');
  });
});
