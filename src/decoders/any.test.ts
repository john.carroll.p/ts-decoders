import { DecoderSuccess } from 'ts-decoders';
import { anyD } from './any';

/**
 * anyD()
 */

describe('anyD()', () => {
  it('init', () => {
    expect(anyD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = anyD();

      for (const item of [true, false, {}, 'false', [], Symbol(), Set]) {
        expect([decoder, item]).toDecodeSuccessfully(new DecoderSuccess(item));
      }
    });
  });
});
