import { Decoder, DecoderSuccess } from 'ts-decoders';
import { ok } from './_util';

/**
 * Creates a decoder which always returns `DecoderSuccess` with
 * whatever input value is provided to it.
 */
export function anyD<T = any>(): Decoder<T> {
  return new Decoder((value): DecoderSuccess<T> => ok(value as T));
}
