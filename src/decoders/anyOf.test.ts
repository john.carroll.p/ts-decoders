import { AsyncDecoder, DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  BOOLEAN_ASYNC_DEC,
  BOOLEAN_DEC,
  NUMBER_DEC,
  STRING_DEC,
} from '../testing';
import { anyOfD } from './anyOf';

/**
 * anyOfD()
 */

describe('anyOfD()', () => {
  it('init', () => {
    expect(anyOfD([])).toBeDecoder();
    expect(anyOfD([STRING_DEC])).toBeDecoder();
    expect(anyOfD([NUMBER_DEC, STRING_DEC])).toBeDecoder();
  });

  describe('normal', () => {
    it('STRING_DEC,BOOLEAN_DEC,NUMBER_DEC', () => {
      const decoder = anyOfD([STRING_DEC, BOOLEAN_DEC, NUMBER_DEC]);

      for (const item of [true, false, 'test', 'one', 1, 23.432, -3432]) {
        expect([decoder, item]).toDecodeSuccessfully(new DecoderSuccess(item));
      }

      for (const item of [{}, null, Symbol('one'), ['false'], undefined]) {
        expect([decoder, item]).toDecodeWithErrors([
          new DecoderError(item, `anyOf 0`, 'invalid value', {
            decoderName: 'anyOfD',
            child: new DecoderError(item, 'invalid type', 'must be a string'),
          }),
          new DecoderError(item, `anyOf 1`, 'invalid value', {
            decoderName: 'anyOfD',
            child: new DecoderError(item, 'invalid type', 'must be a boolean'),
          }),
          new DecoderError(item, `anyOf 2`, 'invalid value', {
            decoderName: 'anyOfD',
            child: new DecoderError(item, 'invalid type', 'must be a number'),
          }),
        ]);
      }
    });
  });

  describe('async', () => {
    it('STRING_DEC,BOOLEAN_DEC,NUMBER_DEC', async () => {
      const decoder = anyOfD([STRING_DEC, BOOLEAN_ASYNC_DEC, NUMBER_DEC]);

      expect(decoder).toBeInstanceOf(AsyncDecoder);

      for (const item of [true, false, 'test', 'one', 1, 23.432, -3432]) {
        await expect([decoder, item]).toAsyncDecodeSuccessfully(
          new DecoderSuccess(item),
        );
      }

      for (const item of [{}, null, Symbol('one'), ['false'], undefined]) {
        await expect([decoder, item]).toAsyncDecodeWithErrors([
          new DecoderError(item, `anyOf 0`, 'invalid value', {
            decoderName: 'anyOfD',
            child: new DecoderError(item, 'invalid type', 'must be a string'),
          }),
          new DecoderError(item, `anyOf 1`, 'invalid value', {
            decoderName: 'anyOfD',
            child: new DecoderError(item, 'invalid type', 'must be a boolean'),
          }),
          new DecoderError(item, `anyOf 2`, 'invalid value', {
            decoderName: 'anyOfD',
            child: new DecoderError(item, 'invalid type', 'must be a number'),
          }),
        ]);
      }
    });
  });
});
