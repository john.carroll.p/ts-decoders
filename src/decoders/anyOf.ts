import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  DecoderReturnType,
  DecoderSuccess,
} from 'ts-decoders';

import {
  applyOptionsToDecoderErrors,
  raceToDecoderSuccess,
  SimpleDecoderOptions,
} from './util';

const decoderName = 'anyOfD';

function childErrors(input: any, children: DecoderError[]): DecoderError[] {
  return children.map(
    (child, index) =>
      new DecoderError(input, `anyOf ${index}`, 'invalid value', {
        child,
      }),
  );
}

export interface AnyOfDecoderOptions extends SimpleDecoderOptions {
  decodeInParallel?: boolean;
}

/**
 * Accepts an array of decoders and attempts to decode a provided value using
 * each of them, in order, returning the first successful result or `DecoderError[]`
 * if all fail. Unlike other decoder functions, `anyOfD()` always returns all
 * errors surfaced by it's decoder arguments. By default, decoder arguments are
 * tried in the order they are given.
 *
 * **Async:**
 * When calling `anyOfD()` with one or more `AsyncDecoder` arguments,
 * you can pass a `decodeInParallel: true` option to specify that a provided value
 * should be tried against all decoder arguments in parallel.
 */

export function anyOfD<T extends Decoder<any>>(
  decoders: T[],
  options?: AnyOfDecoderOptions,
): Decoder<DecoderReturnType<T>>;
export function anyOfD<T extends Decoder<any> | AsyncDecoder<any>>(
  decoders: T[],
  options?: AnyOfDecoderOptions,
): AsyncDecoder<DecoderReturnType<T>>;
export function anyOfD<T extends Decoder<any> | AsyncDecoder<any>>(
  decoders: T[],
  provOptions: AnyOfDecoderOptions = {},
): Decoder<DecoderReturnType<T>> | AsyncDecoder<DecoderReturnType<T>> {
  const options = {
    ...provOptions,
    decoderName: provOptions.decoderName || decoderName,
    allErrors: true,
  } as const;

  const hasAsyncDecoder = decoders.some(
    (decoder): boolean => decoder instanceof AsyncDecoder,
  );

  if (hasAsyncDecoder) {
    if (options.decodeInParallel) {
      return new AsyncDecoder<DecoderReturnType<T>>(
        async (input): Promise<DecoderResult<DecoderReturnType<T>>> => {
          const promises = decoders.map(
            async (decoder): Promise<DecoderResult<DecoderReturnType<T>>> =>
              (await decoder.decode(input)) as DecoderResult<
                DecoderReturnType<T>
              >,
          );

          const result = await raceToDecoderSuccess(promises);

          if (result instanceof DecoderSuccess) return result;

          return applyOptionsToDecoderErrors(
            input,
            childErrors(input, result),
            options,
          );
        },
      );
    }

    return new AsyncDecoder<DecoderReturnType<T>>(
      async (input): Promise<DecoderResult<DecoderReturnType<T>>> => {
        const errors: DecoderError[] = [];

        for (const decoder of decoders) {
          const result = (await decoder.decode(input)) as DecoderResult<
            DecoderReturnType<T>
          >;

          if (result instanceof DecoderSuccess) return result;

          errors.push(...result);
        }

        return applyOptionsToDecoderErrors(
          input,
          childErrors(input, errors),
          options,
        );
      },
    );
  }

  return new Decoder<DecoderReturnType<T>>(
    (input): DecoderResult<DecoderReturnType<T>> => {
      const errors: DecoderError[] = [];

      for (const decoder of decoders) {
        const result = decoder.decode(input) as DecoderResult<
          DecoderReturnType<T>
        >;

        if (result instanceof DecoderSuccess) return result;

        errors.push(...result);
      }

      return applyOptionsToDecoderErrors(
        input,
        childErrors(input, errors),
        options,
      );
    },
  );
}
