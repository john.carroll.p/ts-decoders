import { AsyncDecoder, DecoderError, DecoderSuccess } from 'ts-decoders';
import { STRING_ASYNC_DEC, STRING_DEC } from '../testing';
import { assertDecodesToErrors } from '../testing/_util';
import { arrayD } from './array';

/**
 * arrayD()
 */

describe('arrayD()', () => {
  it('init', () => {
    expect(arrayD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = arrayD();

      for (const item of [[], [1, 'string'], new Array(1000)]) {
        expect([decoder, item]).toDecodeSuccessfully(new DecoderSuccess(item));
      }

      for (const item of [true, false, {}, 'false', Symbol(), Set]) {
        expect([decoder, item]).toDecodeWithErrors([
          new DecoderError(item, 'invalid type', 'must be an array', {
            decoderName: 'arrayD',
          }),
        ]);
      }
    });

    it('STRING_DEC', () => {
      const decoder = arrayD(STRING_DEC);

      const array1 = [1, null];
      assertDecodesToErrors(decoder, array1, [
        new DecoderError(
          array1,
          'invalid element',
          'invalid element [0] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[0]',
            key: 0,
            child: new DecoderError(
              array1[0],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const array2 = ['two', 1, 'green', undefined, new Set()];
      assertDecodesToErrors(decoder, array2, [
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [1] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              array2[1],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('STRING_DEC, {allErrors: true}', () => {
      const decoder = arrayD(STRING_DEC, {
        allErrors: true,
      });

      const array1 = [1, null];
      assertDecodesToErrors(decoder, array1, [
        new DecoderError(
          array1,
          'invalid element',
          'invalid element [0] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[0]',
            key: 0,
            child: new DecoderError(
              array1[0],
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          array1,
          'invalid element',
          'invalid element [1] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              array1[1],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const array2 = ['two', 1, 'green', undefined, new Set()];
      assertDecodesToErrors(decoder, array2, [
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [1] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              array2[1],
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [3] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[3]',
            key: 3,
            child: new DecoderError(
              array1[3],
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [4] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[4]',
            key: 4,
            child: new DecoderError(
              array2[4],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });
  });

  describe('async', () => {
    it('STRING_DEC', async () => {
      const decoder = arrayD(STRING_ASYNC_DEC);

      expect(decoder).toBeInstanceOf(AsyncDecoder);

      const array1 = [1, null];
      await assertDecodesToErrors(decoder, array1, [
        new DecoderError(
          array1,
          'invalid element',
          'invalid element [0] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[0]',
            key: 0,
            child: new DecoderError(
              array1[0],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const array2 = ['two', 1, 'green', undefined, new Set()];
      await assertDecodesToErrors(decoder, array2, [
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [1] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              array2[1],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('STRING_DEC, {allErrors: true}', async () => {
      const decoder = arrayD(STRING_ASYNC_DEC, {
        allErrors: true,
      });

      expect(decoder).toBeInstanceOf(AsyncDecoder);

      const array1 = [1, null];
      await assertDecodesToErrors(decoder, array1, [
        new DecoderError(
          array1,
          'invalid element',
          'invalid element [0] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[0]',
            key: 0,
            child: new DecoderError(
              array1[0],
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          array1,
          'invalid element',
          'invalid element [1] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              array1[1],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const array2 = ['two', 1, 'green', undefined, new Set()];
      await assertDecodesToErrors(decoder, array2, [
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [1] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              array2[1],
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [3] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[3]',
            key: 3,
            child: new DecoderError(
              array2[3],
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          array2,
          'invalid element',
          'invalid element [4] > must be a string',
          {
            decoderName: 'arrayD',
            location: '[4]',
            key: 4,
            child: new DecoderError(
              array2[4],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });
  });
});
