import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
} from 'ts-decoders';
import { errorLocation, ok } from './_util';
import { applyOptionsToDecoderErrors, ComposeDecoderOptions } from './util';

const decoderName = 'arrayD';

function nonArrayError(
  input: any,
  options: ArrayDecoderOptions = {},
): DecoderError[] {
  return applyOptionsToDecoderErrors(
    input,
    new DecoderError(input, 'invalid type', 'must be an array'),
    options,
  );
}

function childError(
  input: any,
  child: DecoderError,
  key: number,
): DecoderError {
  const location = errorLocation(key, child.location);

  return new DecoderError(
    input,
    'invalid element',
    `invalid element [${key}] > ${child.message}`,
    {
      child,
      location,
      key,
    },
  );
}

export type ArrayDecoderOptions = ComposeDecoderOptions;

/**
 * Can be used to make sure an input is an array. If an optional decoder
 * argument is provided, that decoder will be used to process all of the
 * input's elements.
 *
 * Options:
 * - If you pass an `allErrors: true` option as well as any AsyncDecoders
 *   as arguments, then `arrayD()` will create a new AsyncDecoder which
 *   decodes each index of the input array in parallel.
 *
 * Related:
 * - tupleD
 * - dictionaryD
 */

export function arrayD<R = any>(options?: ArrayDecoderOptions): Decoder<R[]>;

export function arrayD<R>(
  decoder: Decoder<R>,
  options?: ArrayDecoderOptions,
): Decoder<R[]>;

export function arrayD<R>(
  decoder: AsyncDecoder<R>,
  options?: ArrayDecoderOptions,
): AsyncDecoder<R[]>;

export function arrayD<R>(
  decoder?: Decoder<R> | AsyncDecoder<R> | ArrayDecoderOptions,
  options: ArrayDecoderOptions = {},
): Decoder<R[]> | AsyncDecoder<R[]> {
  options = {
    ...options,
    decoderName: options.decoderName || decoderName,
  };

  if (!(decoder instanceof Decoder || decoder instanceof AsyncDecoder)) {
    return new Decoder<R[]>(
      (input): DecoderResult<R[]> =>
        Array.isArray(input)
          ? ok<R[]>(input.slice())
          : nonArrayError(input, options),
    );
  }

  if (decoder instanceof AsyncDecoder) {
    if (options.allErrors) {
      return new AsyncDecoder(
        async (input): Promise<DecoderResult<R[]>> => {
          if (!Array.isArray(input)) return nonArrayError(input, options);

          let hasError = false;

          const results = await Promise.all(
            input.map(
              async (item): Promise<DecoderResult<R>> => {
                const result = await decoder.decode(item);

                if (!hasError && areDecoderErrors(result)) {
                  hasError = true;
                }

                return result;
              },
            ),
          );

          if (hasError) {
            const errors: DecoderError[] = [];

            results.forEach((result, index) => {
              if (areDecoderErrors(result)) {
                errors.push(
                  ...result.map(
                    (error): DecoderError => childError(input, error, index),
                  ),
                );
              }
            });

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          const elements = results.map(
            (result): R => (result as DecoderSuccess<R>).value,
          );

          return ok(elements);
        },
      );
    }

    return new AsyncDecoder(
      async (input): Promise<DecoderResult<R[]>> => {
        if (!Array.isArray(input)) return nonArrayError(input, options);

        const elements: R[] = [];
        let index = -1;

        for (const el of input) {
          index++;

          const result = await decoder.decode(el);

          if (areDecoderErrors(result)) {
            const errors = result.map(
              (error): DecoderError => childError(input, error, index),
            );

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          elements.push(result.value);
        }

        return ok(elements);
      },
    );
  }

  return new Decoder(
    (input): DecoderResult<R[]> => {
      if (!Array.isArray(input)) return nonArrayError(input, options);

      const elements: R[] = [];
      let index = -1;

      const allErrors: DecoderError[] = [];

      for (const el of input) {
        index++;

        const result = decoder.decode(el);

        if (areDecoderErrors(result)) {
          const errors = result.map(
            (error): DecoderError => childError(input, error, index),
          );

          if (!options.allErrors) {
            return applyOptionsToDecoderErrors(input, errors, options);
          }

          allErrors.push(...errors);
          continue;
        }

        elements.push(result.value);
      }

      if (allErrors.length > 0) {
        return applyOptionsToDecoderErrors(input, allErrors, options);
      }

      return ok(elements);
    },
  );
}
