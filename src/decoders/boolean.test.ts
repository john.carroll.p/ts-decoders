import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { booleanD } from './boolean';

/**
 * booleanD()
 */

describe('booleanD()', () => {
  it('init', () => {
    expect(booleanD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = booleanD();

      assertDecodesToSuccess(decoder, true, new DecoderSuccess(true));
      assertDecodesToSuccess(decoder, false, new DecoderSuccess(false));

      for (const item of [{}, null, 0, 'false', undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a boolean', {
            decoderName: 'booleanD',
          }),
        ]);
      }
    });
  });
});
