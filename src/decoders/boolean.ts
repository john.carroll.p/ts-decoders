import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

export type BooleanDecoderOptions = SimpleDecoderOptions;

/** Can be used to verify that an unknown value is a `boolean`. */
export function booleanD(
  options: BooleanDecoderOptions = {},
): Decoder<boolean> {
  return new Decoder(
    (value): DecoderResult<boolean> =>
      typeof value === 'boolean'
        ? ok(value)
        : err(value, 'invalid type', 'must be a boolean', 'booleanD', options),
  );
}
