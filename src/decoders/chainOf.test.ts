import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderSuccess,
} from 'ts-decoders';

import { NUMBER_DEC, STRING_DEC } from '../testing';

import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';

import { chainOfD } from './chainOf';

/**
 * chainOfD()
 */

describe('chainOfD()', () => {
  it('init', () => {
    expect(chainOfD(STRING_DEC)).toBeDecoder();
    expect(chainOfD(STRING_DEC, NUMBER_DEC)).toBeDecoder();
  });

  describe('normal', () => {
    it('STRING_DEC,lengthDecoder', () => {
      const lengthDecoder = new Decoder<string, string>(value =>
        value.length > 10
          ? new DecoderSuccess(value)
          : new DecoderError(
              value,
              'invalid length',
              'string must have length greater than 10',
            ),
      );

      const decoder = chainOfD(STRING_DEC, lengthDecoder);

      for (const item of [
        'thisistenletters',
        'morethan10letters',
        'iamelevenle',
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of ['test', 'iameEDvenl', '']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid length',
            'string must have length greater than 10',
            {
              decoderName: 'chainOfD',
              child: new DecoderError(
                item,
                'invalid length',
                'string must have length greater than 10',
              ),
            },
          ),
        ]);
      }

      for (const item of [true, false, 1, 23.432, -3432]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a string', {
            decoderName: 'chainOfD',
            child: new DecoderError(item, 'invalid type', 'must be a string'),
          }),
        ]);
      }
    });
  });

  describe('async', () => {
    it('STRING_DEC,lengthDecoder', async () => {
      const lengthDecoder = new AsyncDecoder<string, string>(async value =>
        value.length > 10
          ? new DecoderSuccess(value)
          : new DecoderError(
              value,
              'invalid length',
              'string must have length greater than 10',
            ),
      );

      const decoder = chainOfD(STRING_DEC, lengthDecoder);

      expect(decoder).toBeAsyncDecoder();

      for (const item of [
        'thisistenletters',
        'morethan10letters',
        'iamelevenle',
      ]) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of ['test', 'iameEDvenl', '']) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid length',
            'string must have length greater than 10',
            {
              decoderName: 'chainOfD',
              child: new DecoderError(
                item,
                'invalid length',
                'string must have length greater than 10',
              ),
            },
          ),
        ]);
      }

      for (const item of [true, false, 1, 23.432, -3432]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a string', {
            decoderName: 'chainOfD',
            child: new DecoderError(item, 'invalid type', 'must be a string'),
          }),
        ]);
      }
    });
  });
});
