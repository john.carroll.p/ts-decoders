import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
} from 'ts-decoders';
import { ok } from './_util';
import { applyOptionsToDecoderErrors, SimpleDecoderOptions } from './util';

const decoderName = 'chainOfD';

function fromChildErrors(input: any, children: DecoderError[]): DecoderError[] {
  return children.map(
    (child): DecoderError =>
      new DecoderError(input, child.type, child.message, {
        decoderName,
        child,
      }),
  );
}

export type ChainOfDecoderOptions = SimpleDecoderOptions;

/**
 * Accepts a spread or array of decoders and attempts to decode a
 * provided value using all of them, in order. The successful output
 * of one decoder is provided as input to the next decoder. `chainOfD()`
 * returns the `DecoderSuccess` value of the last decoder in the chain
 * or `DecoderError` on the first failure. Alternate names for this
 * decoder could be: pipe, compose, or allOf.
 *
 * Related:
 * - `Decoder#chain()`
 */

// The order of these overloads is necessary to work around
// a typescript overload bug
// https://github.com/microsoft/TypeScript/issues/33451

export function chainOfD<A, B, C, D, E, F, G>(
  a: Decoder<B, A>,
  b: Decoder<C, B>,
  c: Decoder<D, C>,
  d: Decoder<E, D>,
  e: Decoder<F, E>,
  f: Decoder<G, F>,
  options?: ChainOfDecoderOptions,
): Decoder<G, A>;
export function chainOfD<A, B, C, D, E, F>(
  a: Decoder<B, A>,
  b: Decoder<C, B>,
  c: Decoder<D, C>,
  d: Decoder<E, D>,
  e: Decoder<F, E>,
  options?: ChainOfDecoderOptions,
): Decoder<F, A>;
export function chainOfD<A, B, C, D, E>(
  a: Decoder<B, A>,
  b: Decoder<C, B>,
  c: Decoder<D, C>,
  d: Decoder<E, D>,
  options?: ChainOfDecoderOptions,
): Decoder<E, A>;
export function chainOfD<A, B, C, D>(
  a: Decoder<B, A>,
  b: Decoder<C, B>,
  c: Decoder<D, C>,
  options?: ChainOfDecoderOptions,
): Decoder<D, A>;
export function chainOfD<A, B, C>(
  a: Decoder<B, A>,
  b: Decoder<C, B>,
  options?: ChainOfDecoderOptions,
): Decoder<C, A>;
export function chainOfD<A, B>(
  a: Decoder<B, A>,
  options?: ChainOfDecoderOptions,
): Decoder<B, A>;
export function chainOfD<A, B = any>(
  decoders: [Decoder<any, A>, ...Array<Decoder<any>>],
  options?: ChainOfDecoderOptions,
): Decoder<B, A>;
export function chainOfD<A, B, C, D, E, F, G>(
  a: Decoder<B, A> | AsyncDecoder<B, A>,
  b: Decoder<C, B> | AsyncDecoder<C, B>,
  c: Decoder<D, C> | AsyncDecoder<D, C>,
  d: Decoder<E, D> | AsyncDecoder<E, D>,
  e: Decoder<F, E> | AsyncDecoder<F, E>,
  f: Decoder<G, F> | AsyncDecoder<G, F>,
  options?: ChainOfDecoderOptions,
): AsyncDecoder<G, A>;
export function chainOfD<A, B, C, D, E, F>(
  a: Decoder<B, A> | AsyncDecoder<B, A>,
  b: Decoder<C, B> | AsyncDecoder<C, B>,
  c: Decoder<D, C> | AsyncDecoder<D, C>,
  d: Decoder<E, D> | AsyncDecoder<E, D>,
  e: Decoder<F, E> | AsyncDecoder<F, E>,
  options?: ChainOfDecoderOptions,
): AsyncDecoder<F, A>;
export function chainOfD<A, B, C, D, E>(
  a: Decoder<B, A> | AsyncDecoder<B, A>,
  b: Decoder<C, B> | AsyncDecoder<C, B>,
  c: Decoder<D, C> | AsyncDecoder<D, C>,
  d: Decoder<E, D> | AsyncDecoder<E, D>,
  options?: ChainOfDecoderOptions,
): AsyncDecoder<E, A>;
export function chainOfD<A, B, C, D>(
  a: Decoder<B, A> | AsyncDecoder<B, A>,
  b: Decoder<C, B> | AsyncDecoder<C, B>,
  c: Decoder<D, C> | AsyncDecoder<D, C>,
  options?: ChainOfDecoderOptions,
): AsyncDecoder<D, A>;
export function chainOfD<A, B, C>(
  a: Decoder<B, A> | AsyncDecoder<B, A>,
  b: Decoder<C, B> | AsyncDecoder<C, B>,
  options?: ChainOfDecoderOptions,
): AsyncDecoder<C, A>;
export function chainOfD<A, B>(
  a: Decoder<B, A> | AsyncDecoder<B, A>,
  options?: ChainOfDecoderOptions,
): AsyncDecoder<B, A>;
export function chainOfD<A, B = any>(
  decoders: [
    Decoder<any, A> | AsyncDecoder<any, A>,
    ...Array<Decoder<any> | AsyncDecoder<any>>,
  ],
  options?: ChainOfDecoderOptions,
): AsyncDecoder<B, A>;
export function chainOfD(
  a: Decoder<any> | AsyncDecoder<any> | Array<Decoder<any> | AsyncDecoder<any>>,
  b?: Decoder<any> | AsyncDecoder<any> | ChainOfDecoderOptions,
  c?: Decoder<any> | AsyncDecoder<any> | ChainOfDecoderOptions,
  d?: Decoder<any> | AsyncDecoder<any> | ChainOfDecoderOptions,
  e?: Decoder<any> | AsyncDecoder<any> | ChainOfDecoderOptions,
  f?: Decoder<any> | AsyncDecoder<any> | ChainOfDecoderOptions,
  o?: ChainOfDecoderOptions,
): any {
  let options: ChainOfDecoderOptions | undefined;
  const decoders: Array<Decoder<any> | AsyncDecoder<any>> = [];

  if (Array.isArray(a)) decoders.push(...a);
  else decoders.push(a);

  [b, c, d, e, f, o].forEach(val => {
    if (isDecoder(val)) decoders.push(val);
    else if (val) options = val;
  });

  options = options || {};
  options = { ...options, decoderName: options.decoderName || decoderName };

  if (decoders.some((decoder): boolean => decoder instanceof AsyncDecoder)) {
    return new AsyncDecoder(
      async (input): Promise<DecoderResult<any>> => {
        let result = ok(input as unknown) as DecoderResult<any>;

        for (const decoder of decoders) {
          if (!(result instanceof DecoderSuccess)) break;

          result = (await decoder.decode(result.value)) as DecoderResult<any>;
        }

        if (areDecoderErrors(result)) {
          return applyOptionsToDecoderErrors(
            input,
            fromChildErrors(input, result),
            options,
          );
        }

        return result;
      },
    );
  }

  return new Decoder(input => {
    const result = decoders.reduce(
      (prev, curr) => {
        return (prev instanceof DecoderSuccess
          ? curr.decode(prev.value)
          : prev) as DecoderResult<any>;
      },
      ok(input as unknown) as DecoderResult<any>,
    );

    if (areDecoderErrors(result)) {
      return applyOptionsToDecoderErrors(
        input,
        fromChildErrors(input, result),
        options,
      );
    }

    return result;
  });
}

function isDecoder(a?: unknown): a is Decoder<any> | AsyncDecoder<any> {
  return a instanceof Decoder || a instanceof AsyncDecoder;
}
