import { DecoderSuccess } from 'ts-decoders';
import { assertDecodesToSuccess } from '../testing/_util';
import { constantD } from './constant';

/**
 * constantD()
 */

describe('constantD()', () => {
  it('init', () => {
    expect(constantD(0)).toBeDecoder();
    expect(constantD(1)).toBeDecoder();
    expect(constantD('0')).toBeDecoder();
    expect(constantD({})).toBeDecoder();
  });

  describe('normal', () => {
    it('0', () => {
      const decoder = constantD(0);

      for (const item of [true, false, {}, 'false']) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(0));
      }
    });

    it('"one"', () => {
      const decoder = constantD('one');

      for (const item of [true, false, {}, 'false']) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess('one'));
      }
    });

    it('{}', () => {
      const obj = {};
      const decoder = constantD(obj);

      for (const item of [true, false, {}, 'false']) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(obj));
      }
    });
  });
});
