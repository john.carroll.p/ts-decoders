import { Decoder, DecoderSuccess } from 'ts-decoders';
import { ok } from './_util';

/**
 * Accepts a `value: T` argument and creates a decoder which
 * always returns `DecoderSuccess<T>` with the provided
 * `value` argument, ignoring its input.
 */
export function constantD<T extends string | number | bigint | boolean>(
  exact: T,
): Decoder<T>;
export function constantD<T>(exact: T): Decoder<T>;
export function constantD<T>(constant: T): Decoder<T> {
  return new Decoder((): DecoderSuccess<T> => ok(constant));
}
