import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderSuccess,
} from 'ts-decoders';
import { STRING_ASYNC_DEC, STRING_DEC } from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { dictionaryD } from './dictionary';

/**
 * dictionaryD()
 */

describe('dictionaryD()', () => {
  it('init', () => {
    expect(dictionaryD(STRING_DEC)).toBeDecoder();
    expect(dictionaryD(STRING_DEC, STRING_DEC)).toBeDecoder();
  });

  describe('normal', () => {
    it('STRING_DEC', () => {
      const decoder = dictionaryD(STRING_DEC);

      for (const item of [
        { 1: 'one' },
        { 1: 'one', two: 'two' },
        { three: 'a name', four: 'another name', 'a longer key': 'name' },
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'dictionaryD',
          }),
        ]);
      }

      const input = {
        1: 'one',
        two: ['two'],
        three: null,
        four: true,
        'a longer key': 6,
      };

      assertDecodesToErrors(decoder, input, [
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["two"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'two',
            key: 'two',
            child: new DecoderError(
              input.two,
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('STRING_DEC,{allErrors: true}', () => {
      const decoder = dictionaryD(STRING_DEC, { allErrors: true });

      for (const item of [
        { 1: 'one' },
        { 1: 'one', two: 'two' },
        { three: 'a name', four: 'another name', 'a longer key': 'name' },
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'dictionaryD',
          }),
        ]);
      }

      const input = {
        1: 'one',
        two: ['two'],
        three: null,
        four: true,
        'a longer key': 6,
      };

      assertDecodesToErrors(decoder, input, [
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["two"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'two',
            key: 'two',
            child: new DecoderError(
              input.two,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["three"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'three',
            key: 'three',
            child: new DecoderError(
              input.three,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["four"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'four',
            key: 'four',
            child: new DecoderError(
              input.four,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["a longer key"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: '["a longer key"]',
            key: 'a longer key',
            child: new DecoderError(
              input['a longer key'],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('STRING_DEC,keyDecoder', () => {
      const keyDecoder = new Decoder((value: string) =>
        value.length < 5
          ? new DecoderSuccess(value)
          : new DecoderError(
              value,
              'invalid length',
              'must have length less than 5',
            ),
      );

      const decoder = dictionaryD(STRING_DEC, keyDecoder);

      for (const item of [{ 1: 'one' }, { 1: 'one', two: 'two' }]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'dictionaryD',
          }),
        ]);
      }

      const input = {
        1: 'one',
        two: ['two'],
        three: null,
        four: true,
        'a longer key': 6,
      };

      assertDecodesToErrors(decoder, input, [
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["two"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'two',
            key: 'two',
            child: new DecoderError(
              input.two,
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('STRING_DEC,keyDecoder,{allErrors: true}', () => {
      const keyDecoder = new Decoder((value: string) =>
        value.length < 5
          ? new DecoderSuccess(value)
          : new DecoderError(
              value,
              'invalid length',
              'must have length less than 5',
            ),
      );

      const decoder = dictionaryD(STRING_DEC, keyDecoder, {
        allErrors: true,
      });

      for (const item of [{ 1: 'one' }, { 1: 'one', two: 'two' }]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'dictionaryD',
          }),
        ]);
      }

      const input = {
        1: 'one',
        two: ['two'],
        three: null,
        four: true,
        'a longer key': 6,
      };

      assertDecodesToErrors(decoder, input, [
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["two"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'two',
            key: 'two',
            child: new DecoderError(
              input.two,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key',
          'invalid key ["three"] > must have length less than 5',
          {
            decoderName: 'dictionaryD',
            location: 'three',
            key: 'three',
            child: new DecoderError(
              'three',
              'invalid length',
              'must have length less than 5',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["four"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'four',
            key: 'four',
            child: new DecoderError(
              input.four,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key',
          'invalid key ["a longer key"] > must have length less than 5',
          {
            decoderName: 'dictionaryD',
            location: '["a longer key"]',
            key: 'a longer key',
            child: new DecoderError(
              'a longer key',
              'invalid length',
              'must have length less than 5',
            ),
          },
        ),
      ]);
    });
  });

  describe('async', () => {
    it('STRING_DEC,keyDecoder', async () => {
      const keyAsyncDecoder = new AsyncDecoder(async (value: string) =>
        value.length < 5
          ? new DecoderSuccess(value)
          : new DecoderError(
              value,
              'invalid length',
              'must have length less than 5',
            ),
      );

      const decoder = dictionaryD(STRING_ASYNC_DEC, keyAsyncDecoder);

      for (const item of [{ 1: 'one' }, { 1: 'one', two: 'two' }]) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'dictionaryD',
          }),
        ]);
      }

      const input = {
        1: 'one',
        two: ['two'],
        three: null,
        four: true,
        'a longer key': 6,
      };

      await assertDecodesToErrors(decoder, input, [
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["two"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'two',
            key: 'two',
            child: new DecoderError(
              input.two,
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('STRING_DEC,keyDecoder,{allErrors: true}', async () => {
      const keyAsyncDecoder = new AsyncDecoder(async (value: string) =>
        value.length < 5
          ? new DecoderSuccess(value)
          : new DecoderError(
              value,
              'invalid length',
              'must have length less than 5',
            ),
      );

      const decoder = dictionaryD(STRING_DEC, keyAsyncDecoder, {
        allErrors: true,
      });

      expect(decoder).toBeAsyncDecoder();

      for (const item of [{ 1: 'one' }, { 1: 'one', two: 'two' }]) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'dictionaryD',
          }),
        ]);
      }

      const input = {
        1: 'one',
        two: ['two'],
        three: null,
        four: true,
        'a longer key': 6,
      };

      await assertDecodesToErrors(decoder, input, [
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["two"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'two',
            key: 'two',
            child: new DecoderError(
              input.two,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key',
          'invalid key ["three"] > must have length less than 5',
          {
            decoderName: 'dictionaryD',
            location: 'three',
            key: 'three',
            child: new DecoderError(
              'three',
              'invalid length',
              'must have length less than 5',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key value',
          'invalid value for key ["four"] > must be a string',
          {
            decoderName: 'dictionaryD',
            location: 'four',
            key: 'four',
            child: new DecoderError(
              input.four,
              'invalid type',
              'must be a string',
            ),
          },
        ),
        new DecoderError(
          input,
          'invalid key',
          'invalid key ["a longer key"] > must have length less than 5',
          {
            decoderName: 'dictionaryD',
            location: '["a longer key"]',
            key: 'a longer key',
            child: new DecoderError(
              'a longer key',
              'invalid length',
              'must have length less than 5',
            ),
          },
        ),
      ]);
    });
  });
});
