import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
} from 'ts-decoders';
import { errorLocation, ok } from './_util';
import { applyOptionsToDecoderErrors, ComposeDecoderOptions } from './util';

const decoderName = 'dictionaryD';

function childKeyError(
  input: any,
  child: DecoderError,
  key: string,
): DecoderError {
  const location = errorLocation(key, '');

  return new DecoderError(
    input,
    'invalid key',
    `invalid key ["${key}"] > ${child.message}`,
    {
      decoderName,
      child,
      location,
      key,
    },
  );
}

function childValueError(
  input: any,
  child: DecoderError,
  key: string,
): DecoderError {
  const location = errorLocation(key, child.location);

  return new DecoderError(
    input,
    'invalid key value',
    `invalid value for key ["${key}"] > ${child.message}`,
    {
      decoderName,
      child,
      location,
      key,
    },
  );
}

async function asyncDecodeKey(
  input: any,
  decoder: Decoder<string, string> | AsyncDecoder<string, string>,
  key: string,
): Promise<DecoderResult<string>> {
  const keyResult = await decoder.decode(key);

  if (areDecoderErrors(keyResult)) {
    return keyResult.map(error => childKeyError(input, error, key));
  }

  return keyResult;
}

async function asyncDecodeValue<R, V>(
  input: any,
  decoder: Decoder<R, V> | AsyncDecoder<R, V>,
  value: V,
  key: string,
): Promise<DecoderResult<R>> {
  const valueResult = await decoder.decode(value);

  if (areDecoderErrors(valueResult)) {
    return valueResult.map(error => childValueError(input, error, key));
  }

  return valueResult;
}

function nonObjectError(
  input: any,
  options?: DictionaryDecoderOptions,
): DecoderError[] {
  return applyOptionsToDecoderErrors(
    input,
    new DecoderError(input, 'invalid type', 'must be a non-null object', {
      decoderName,
    }),
    options,
  );
}

export type DictionaryDecoderOptions = ComposeDecoderOptions;

/**
 * Receives a decoder argument and uses that decoder to process all
 * values (regardless of key) of an input object. You can pass an
 * optional key decoder as the second argument which will be used to
 * decode each key of an input object.
 *
 * Options:
 * - If you pass an `allErrors: true` option as well as any
 * AsyncDecoders as arguments, then `dictionaryD()` will create a new
 * AsyncDecoder which decodes each key of the input object in parallel.
 *
 * Related:
 * - objectD
 * - arrayD
 */
export function dictionaryD<R, V>(
  valueDecoder: Decoder<R, V>,
  options?: DictionaryDecoderOptions,
): Decoder<{ [key: string]: R }, V>;
export function dictionaryD<R, V>(
  valueDecoder: Decoder<R, V>,
  keyDecoder: Decoder<string, string>,
  options?: DictionaryDecoderOptions,
): Decoder<{ [key: string]: R }, V>;
export function dictionaryD<R, V>(
  decoder: AsyncDecoder<R, V>,
  options?: DictionaryDecoderOptions,
): AsyncDecoder<{ [key: string]: R }, V>;
export function dictionaryD<R, V>(
  valueDecoder: Decoder<R, V> | AsyncDecoder<R, V>,
  keyDecoder: Decoder<string, string> | AsyncDecoder<string, string>,
  options?: DictionaryDecoderOptions,
): AsyncDecoder<{ [key: string]: R }, V>;
export function dictionaryD<R, V>(
  decoder: Decoder<R, V> | AsyncDecoder<R, V>,
  optionalA?:
    | DictionaryDecoderOptions
    | Decoder<string, string>
    | AsyncDecoder<string, string>,
  optionalB?: DictionaryDecoderOptions,
): Decoder<{ [key: string]: R }, V> | AsyncDecoder<{ [key: string]: R }, V> {
  let keyDecoder:
    | Decoder<string, string>
    | AsyncDecoder<string, string>
    | undefined;
  let options: DictionaryDecoderOptions = {};

  if (optionalA) {
    if (optionalA instanceof Decoder || optionalA instanceof AsyncDecoder) {
      keyDecoder = optionalA;
    } else {
      options = optionalA;
    }
  }

  if (optionalB) {
    options = optionalB;
  }

  options = {
    ...options,
    decoderName: options.decoderName || decoderName,
  };

  if (decoder instanceof AsyncDecoder || keyDecoder instanceof AsyncDecoder) {
    if (options.allErrors) {
      return new AsyncDecoder(
        async (
          input: any,
        ): Promise<
          DecoderResult<{
            [x: string]: R;
          }>
        > => {
          if (typeof input !== 'object' || input === null) {
            return nonObjectError(input, options);
          }

          let hasError = false;

          const results = await Promise.all(
            Object.entries(input as { [key: string]: V }).map(
              async ([entryKey, entryValue]): Promise<
                DecoderError[] | [string, R]
              > => {
                let key = entryKey;

                if (keyDecoder) {
                  const keyResult = await asyncDecodeKey(
                    input,
                    keyDecoder!,
                    entryKey,
                  );

                  if (Array.isArray(keyResult)) {
                    hasError = true;
                    return keyResult;
                  }

                  key = keyResult.value;
                }

                const valueResult = await asyncDecodeValue(
                  input,
                  decoder,
                  entryValue,
                  entryKey,
                );

                if (Array.isArray(valueResult)) {
                  hasError = true;
                  return valueResult;
                }

                return [key, valueResult.value] as [string, R];
              },
            ),
          );

          if (hasError) {
            const errors: DecoderError[] = [];

            results.forEach((result): void => {
              if (result[0] instanceof DecoderError) {
                errors.push(...(result as DecoderError[]));
              }
            });

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          return ok(Object.fromEntries(results as Array<[string, R]>));
        },
      );
    }

    return new AsyncDecoder(
      async (
        input: V,
      ): Promise<
        DecoderResult<{
          [key: string]: R;
        }>
      > => {
        if (typeof input !== 'object' || input === null) {
          return nonObjectError(input, options);
        }

        const resultObject: { [key: string]: R } = {};

        for (const [entryKey, entryValue] of Object.entries(input)) {
          let key = entryKey;

          if (keyDecoder) {
            const keyResult = await asyncDecodeKey(
              input,
              keyDecoder!,
              entryKey,
            );

            if (Array.isArray(keyResult)) {
              return applyOptionsToDecoderErrors(input, keyResult, options);
            }

            key = keyResult.value;
          }

          const valueResult = await asyncDecodeValue(
            input,
            decoder,
            entryValue,
            entryKey,
          );

          if (Array.isArray(valueResult)) {
            return applyOptionsToDecoderErrors(input, valueResult, options);
          }

          resultObject[key] = valueResult.value;
        }

        return ok(resultObject);
      },
    );
  }

  return new Decoder(
    (
      input: V,
    ): DecoderResult<{
      [key: string]: R;
    }> => {
      if (typeof input !== 'object' || input === null) {
        return nonObjectError(input, options);
      }

      const resultObject: { [key: string]: R } = {};

      const entries: Array<[string, V]> = Object.entries(input);
      const allErrors: DecoderError[] = [];

      for (const [entryKey, entryValue] of entries) {
        let key = entryKey;

        if (keyDecoder) {
          const keyResult = (keyDecoder as Decoder<string, string>).decode(
            entryKey,
          );

          if (areDecoderErrors(keyResult)) {
            const errors = keyResult.map(
              (error): DecoderError => childKeyError(input, error, entryKey),
            );

            if (options.allErrors) {
              allErrors.push(...errors);
              continue;
            }

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          key = keyResult.value;
        }

        const valueResult = decoder.decode(entryValue);

        if (areDecoderErrors(valueResult)) {
          const errors = valueResult.map(
            (error): DecoderError => childValueError(input, error, entryKey),
          );

          if (options.allErrors) {
            allErrors.push(...errors);
            continue;
          }

          return applyOptionsToDecoderErrors(input, errors, options);
        }

        resultObject[key] = valueResult.value;
      }

      if (allErrors.length > 0) {
        return applyOptionsToDecoderErrors(input, allErrors, options);
      }

      return ok(resultObject);
    },
  );
}
