import { matchD } from './match';
import { SimpleDecoderOptions } from './util';

// this email regex is taken from this S.O. answer
// https://dba.stackexchange.com/a/165923/191558
// which in turn takes it from the HTML5 spec
const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

export type EmailDecoderOptions = SimpleDecoderOptions;

/** Can be used to verify that an unknown value is an email address `string`. */
export function emailD(options: EmailDecoderOptions = {}) {
  return matchD(emailRegex, {
    data: options.data,
    decoderName: options.decoderName || 'emailD',
    errorMsg: options.errorMsg || 'must be a email address',
  });
}
