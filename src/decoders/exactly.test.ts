import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { exactlyD } from './exactly';

/**
 * exactlyD()
 */

describe('exactlyD()', () => {
  it('init', () => {
    expect(exactlyD(0)).toBeDecoder();
    expect(exactlyD(1)).toBeDecoder();
    expect(exactlyD('0')).toBeDecoder();
    expect(exactlyD({})).toBeDecoder();
  });

  describe('normal', () => {
    it('1', () => {
      const decoder = exactlyD(1);

      assertDecodesToSuccess(decoder, 1, new DecoderSuccess(1));

      for (const item of [true, undefined, 0, 'true', {}]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid value', 'must be 1', {
            decoderName: 'exactlyD',
          }),
        ]);
      }
    });

    it('undefined', () => {
      const decoder = exactlyD(undefined);

      assertDecodesToSuccess(decoder, undefined, new DecoderSuccess(undefined));

      for (const item of [true, null, 0, 'true', {}]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid value', 'must be undefined', {
            decoderName: 'exactlyD',
          }),
        ]);
      }
    });

    it('null', () => {
      const decoder = exactlyD(null);

      assertDecodesToSuccess(decoder, null, new DecoderSuccess(null));

      for (const item of [true, undefined, 0, 'true', {}]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid value', 'must be null', {
            decoderName: 'exactlyD',
          }),
        ]);
      }
    });

    it('{}', () => {
      const obj = {};
      const decoder = exactlyD(obj);

      assertDecodesToSuccess(decoder, obj, new DecoderSuccess(obj));

      for (const item of [true, undefined, 0, 'true', {}]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid value', 'must be [object Object]', {
            decoderName: 'exactlyD',
          }),
        ]);
      }
    });
  });
});
