import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

export type ExactlyDecoderOptions = SimpleDecoderOptions;

/**
 * Accepts a `value` argument and can be used to verify that
 * an unknown input is `=== value`.
 */
export function exactlyD<T extends string | number | bigint | boolean>(
  exact: T,
  options?: ExactlyDecoderOptions,
): Decoder<T>;
export function exactlyD<T>(
  exact: T,
  options?: ExactlyDecoderOptions,
): Decoder<T>;
export function exactlyD<T>(
  exact: T,
  options: ExactlyDecoderOptions = {},
): Decoder<T> {
  return new Decoder(
    (value): DecoderResult<T> =>
      value === exact
        ? ok(value as T)
        : err(value, 'invalid value', `must be ${exact}`, 'exactlyD', options),
  );
}
