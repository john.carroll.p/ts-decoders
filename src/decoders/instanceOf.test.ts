import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { instanceOfD } from './instanceOf';

/**
 * instanceOfD()
 */

describe('instanceOfD()', () => {
  it('init', () => {
    expect(instanceOfD(Map)).toBeDecoder();
    expect(instanceOfD(Array)).toBeDecoder();
    expect(instanceOfD(Set)).toBeDecoder();
  });

  describe('normal', () => {
    it('Map', () => {
      const map = new Map();
      const set = new Set();
      const array: unknown[] = [];
      const mapDecoder = instanceOfD(Map);

      assertDecodesToSuccess(mapDecoder, map, new DecoderSuccess(map));

      for (const item of [{}, null, 0, undefined, 'str', set, array]) {
        assertDecodesToErrors(mapDecoder, item, [
          new DecoderError(
            item,
            'invalid class',
            'must be an instance of Map',
            {
              decoderName: 'instanceOfD',
            },
          ),
        ]);
      }
    });

    it('Set', () => {
      const map = new Map();
      const set = new Set();
      const array: unknown[] = [];
      const mapDecoder = instanceOfD(Set);

      assertDecodesToSuccess(mapDecoder, set, new DecoderSuccess(set));

      for (const item of [{}, null, 0, undefined, 'str', map, array]) {
        assertDecodesToErrors(mapDecoder, item, [
          new DecoderError(
            item,
            'invalid class',
            'must be an instance of Set',
            {
              decoderName: 'instanceOfD',
            },
          ),
        ]);
      }
    });

    it('Array', () => {
      const map = new Map();
      const set = new Set();
      const array: unknown[] = [];
      const mapDecoder = instanceOfD(Array);

      assertDecodesToSuccess(mapDecoder, array, new DecoderSuccess(array));

      for (const item of [{}, null, 0, undefined, 'str', map, set]) {
        assertDecodesToErrors(mapDecoder, item, [
          new DecoderError(
            item,
            'invalid class',
            'must be an instance of Array',
            {
              decoderName: 'instanceOfD',
            },
          ),
        ]);
      }
    });
  });
});
