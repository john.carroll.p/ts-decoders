import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

export type InstanceOfDecoderOptions = SimpleDecoderOptions;

/**
 * Accepts a javascript constructor argument (`class`) and creates a
 * decoder which verifies that its input is an `instanceof` the
 * provided constructor.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function instanceOfD<T extends new (...args: any) => any>(
  clazz: T,
  options: InstanceOfDecoderOptions = {},
): Decoder<InstanceType<T>> {
  return new Decoder(
    (value): DecoderResult<InstanceType<T>> =>
      value instanceof clazz
        ? ok(value as InstanceType<T>)
        : err(
            value,
            'invalid class',
            `must be an instance of ${clazz.name}`,
            'instanceOfD',
            options,
          ),
  );
}
