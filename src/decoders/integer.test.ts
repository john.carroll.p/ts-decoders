import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { integerD } from './integer';

/**
 * integerD()
 */

describe('integerD()', () => {
  it('init', () => {
    expect(integerD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = integerD();

      for (const item of [0, -14, 100, 4448928342948]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [
        0.123,
        -342.342342,
        3432432.4,
        {},
        null,
        undefined,
        'str',
      ]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a whole number', {
            decoderName: 'integerD',
          }),
        ]);
      }
    });
  });
});
