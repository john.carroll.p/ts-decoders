import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

export type IntegerDecoderOptions = SimpleDecoderOptions;

/**
 * Can be used to verify that an unknown value is a whole `number`.
 *
 * Related:
 * - numberD
 * */
export function integerD(options: IntegerDecoderOptions = {}): Decoder<number> {
  return new Decoder(
    (value): DecoderResult<number> =>
      Number.isInteger(value)
        ? ok(value as number)
        : err(
            value,
            'invalid type',
            'must be a whole number',
            'integerD',
            options,
          ),
  );
}
