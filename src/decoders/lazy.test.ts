import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
} from 'ts-decoders';
import { NestedArray, STRING_DEC } from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { lazyD } from './lazy';

const decoder: Decoder<NestedArray[]> = new Decoder(input => {
  if (!Array.isArray(input)) {
    return new DecoderError(input, 'invalid type', 'must be an array');
  }

  let index = -1;
  const array: NestedArray[] = [];

  for (const element of input) {
    index++;
    const result = lazyD((): Decoder<NestedArray[]> => decoder).decode(element);

    if (areDecoderErrors(result)) {
      return Array.from(result.values()).map(
        error =>
          new DecoderError(
            input,
            'invalid element',
            `invalid element [${index}] > ${error.message}`,
            {
              location: `[${index}]${error.location}`,
              key: index,
              child: error,
            },
          ),
      );
    }

    array.push(result.value);
  }

  return new DecoderSuccess(array);
});

/**
 * lazyD()
 */

describe('lazyD()', () => {
  it('init', () => {
    expect(lazyD((): Decoder<string> => STRING_DEC)).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      for (const item of [[], [[]], [[], [], [[], [], []]]]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      const obj1 = [[], [], [[], [true], []]];
      const msg =
        `invalid element [2] > invalid element [1] > ` +
        `invalid element [0] > must be an array`;

      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(obj1, 'invalid element', msg, {
          location: '[2][1][0]',
          key: 2,
          child: new DecoderError(
            obj1[2],
            'invalid element',
            'invalid element [1] > invalid element [0] > must be an array',
            {
              location: '[1][0]',
              key: 1,
              child: new DecoderError(
                obj1[2][1],
                'invalid element',
                'invalid element [0] > must be an array',
                {
                  location: '[0]',
                  key: 0,
                  child: new DecoderError(
                    obj1[2][1][0],
                    'invalid type',
                    'must be an array',
                    {
                      location: '',
                    },
                  ),
                },
              ),
            },
          ),
        }),
      ]);
    });
  });

  describe('async', () => {
    it('', async () => {
      const asyncDecoder = new AsyncDecoder(
        async (value): Promise<DecoderResult<NestedArray[]>> =>
          decoder.decode(value),
      );

      for (const item of [[], [[]], [[], [], [[], [], []]]]) {
        await assertDecodesToSuccess(
          asyncDecoder,
          item,
          new DecoderSuccess(item),
        );
      }

      const obj1 = [[], [], [[], [true], []]];
      const msg =
        `invalid element [2] > invalid element [1] > ` +
        `invalid element [0] > must be an array`;

      await assertDecodesToErrors(asyncDecoder, obj1, [
        new DecoderError(obj1, 'invalid element', msg, {
          location: '[2][1][0]',
          key: 2,
          child: new DecoderError(
            obj1[2],
            'invalid element',
            'invalid element [1] > invalid element [0] > must be an array',
            {
              location: '[1][0]',
              key: 1,
              child: new DecoderError(
                obj1[2][1],
                'invalid element',
                'invalid element [0] > must be an array',
                {
                  location: '[0]',
                  key: 0,
                  child: new DecoderError(
                    obj1[2][1][0],
                    'invalid type',
                    'must be an array',
                    {
                      location: '',
                    },
                  ),
                },
              ),
            },
          ),
        }),
      ]);
    });
  });
});
