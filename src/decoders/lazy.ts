import { AsyncDecoder, Decoder, DecoderResult } from 'ts-decoders';

export interface LazyDecoderOptions {
  decoderName?: string;
  promise?: boolean;
}

/**
 * Recieves a function which returns a decoder and creates a new
 * decoder which calls this function on each `.decode()` call and
 * uses the returned decoder to decode it's input. A common use case
 * for this decoder is to decode recursive data structures.
 * Alternate names for this decoder could be: recursive.
 */
export function lazyD<R, I = any>(
  decoderFn: (value: I) => Decoder<R, I>,
  options?: LazyDecoderOptions & { promise?: false },
): Decoder<R, I>;
export function lazyD<R, I = any>(
  decoderFn: (
    value: I,
  ) =>
    | Decoder<R, I>
    | AsyncDecoder<R, I>
    | Promise<Decoder<R, I> | AsyncDecoder<R, I>>,
  options: LazyDecoderOptions & { promise: true },
): AsyncDecoder<R, I>;
export function lazyD<R, I = any>(
  decoderFn: (
    value: I,
  ) =>
    | Decoder<R, I>
    | AsyncDecoder<R, I>
    | Promise<Decoder<R, I> | AsyncDecoder<R, I>>,
  options: LazyDecoderOptions = {},
): Decoder<R, I> | AsyncDecoder<R, I> {
  if (options.promise) {
    return new AsyncDecoder(
      async (value): Promise<DecoderResult<R>> => {
        const decoder = await decoderFn(value);
        return decoder.decode(value);
      },
    );
  }

  return new Decoder(
    (value): DecoderResult<R> =>
      (decoderFn(value) as Decoder<R, I>).decode(value),
  );
}
