import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { matchD } from './match';

/**
 * matchD()
 */

describe('matchD()', () => {
  it('init', () => {
    expect(matchD(/one/)).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      // prettier-ignore
      const regex =
      /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;

      const decoder = matchD(regex);

      for (const item of ['2019-07-03', '2000-01-01', '0432-11-30']) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, -342.342342, {}, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a string', {
            decoderName: 'matchD',
          }),
        ]);
      }

      for (const item of [
        '01-01-2019',
        '2000-00-01',
        '04321-30',
        '',
        '342-43234342',
      ]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid value',
            `must be a string matching the pattern "${regex}"`,
            {
              decoderName: 'matchD',
            },
          ),
        ]);
      }
    });
  });
});
