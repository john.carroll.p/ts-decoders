import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

const decoderName = 'matchD';

export type MatchDecoderOptions = SimpleDecoderOptions;

/**
 * Can be used to verify that an unknown value is a `string`
 * which conforms to the given `RegExp`.
 */
export function matchD(
  regex: RegExp,
  options: MatchDecoderOptions = {},
): Decoder<string> {
  return new Decoder(
    (value): DecoderResult<string> => {
      if (typeof value !== 'string') {
        return err(
          value,
          'invalid type',
          `must be a string`,
          decoderName,
          options,
        );
      }

      return regex.test(value)
        ? ok(value)
        : err(
            value,
            'invalid value',
            `must be a string matching the pattern "${regex}"`,
            decoderName,
            options,
          );
    },
  );
}
