import { DecoderError } from 'ts-decoders';
import { assertDecodesToErrors } from '../testing/_util';
import { neverD } from './never';

/**
 * neverD()
 */

describe('neverD()', () => {
  it('', () => {
    expect(neverD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = neverD();

      for (const item of [1, -342.342342, {}, null, undefined, 'str', true]) {
        assertDecodesToErrors(decoder as any, item, [
          new DecoderError(item, 'forbidden', 'must not be present', {
            decoderName: 'neverD',
          }),
        ]);
      }
    });
  });
});
