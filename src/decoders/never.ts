import { Decoder } from 'ts-decoders';
import { err } from './_util';
import { SimpleDecoderOptions } from './util';

export type NeverDecoderOptions = SimpleDecoderOptions;

/**
 * Creates a decoder which always returns `DecoderError` with
 * whatever input value is provided to it.
 */
export function neverD(options: NeverDecoderOptions = {}): Decoder<never> {
  return new Decoder<never>(value =>
    err(value, 'forbidden', 'must not be present', 'neverD', options),
  );
}
