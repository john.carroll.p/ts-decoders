import { DecoderError, DecoderSuccess } from 'ts-decoders';
import { STRING_ASYNC_DEC, STRING_DEC } from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { nullableD } from './nullable';

/**
 * nullableD()
 */

describe('nullableD()', () => {
  it('init', () => {
    expect(nullableD(STRING_DEC)).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = nullableD(STRING_DEC);

      for (const item of ['2019-07-03', 'heLLooooo', null]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, {}, true, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid type',
            'must be a string OR must be null',
            {
              decoderName: 'nullableD',
              child: new DecoderError(item, 'invalid type', 'must be a string'),
            },
          ),
        ]);
      }
    });
  });

  describe('async', () => {
    it('', async () => {
      const decoder = nullableD(STRING_ASYNC_DEC);

      expect(decoder).toBeAsyncDecoder();

      for (const item of ['2019-07-03', 'heLLooooo', null]) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, {}, true, undefined]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid type',
            'must be a string OR must be null',
            {
              decoderName: 'nullableD',
              child: new DecoderError(item, 'invalid type', 'must be a string'),
            },
          ),
        ]);
      }
    });
  });
});
