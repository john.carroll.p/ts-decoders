import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  isDecoderSuccess,
} from 'ts-decoders';
import { exactlyD } from './exactly';
import { applyOptionsToDecoderErrors, SimpleDecoderOptions } from './util';

const decoderName = 'nullableD';
const nullDecoder = exactlyD(null);

export type NullableDecoderOptions = SimpleDecoderOptions;

/**
 * Accepts a decoder and returns a new decoder which accepts either the
 * original decoder's value or `null`.
 *
 * Related:
 * - undefinableD
 * - optionalD
 */
export function nullableD<R, I>(
  decoder: Decoder<R, I>,
  options?: NullableDecoderOptions,
): Decoder<R | null, I>;
export function nullableD<R, I>(
  decoder: AsyncDecoder<R, I>,
  options?: NullableDecoderOptions,
): AsyncDecoder<R | null, I>;
export function nullableD<R, I>(
  decoder: Decoder<R, I> | AsyncDecoder<R, I>,
  options: NullableDecoderOptions = {},
): Decoder<R | null, I> | AsyncDecoder<R | null, I> {
  if (decoder instanceof AsyncDecoder) {
    return new AsyncDecoder(
      async (input: I): Promise<DecoderResult<R | null>> => {
        let result: DecoderResult<R | null> = nullDecoder.decode(input);

        if (isDecoderSuccess(result)) return result;

        result = await decoder.decode(input);

        if (isDecoderSuccess(result)) return result;

        return applyOptionsToDecoderErrors(
          input,
          Array.from(result.values()).map(
            error =>
              new DecoderError(
                input,
                'invalid type',
                `${error.message} OR must be null`,
                {
                  child: error,
                  decoderName,
                },
              ),
          ),
          options,
        );
      },
    );
  }

  return new Decoder(
    (input: I): DecoderResult<R | null> => {
      let result: DecoderResult<R | null> = nullDecoder.decode(input);

      if (isDecoderSuccess(result)) return result;

      result = decoder.decode(input);

      if (isDecoderSuccess(result)) return result;

      return applyOptionsToDecoderErrors(
        input,
        Array.from(result.values()).map(
          error =>
            new DecoderError(
              input,
              'invalid type',
              `${error.message} OR must be null`,
              {
                child: error,
                decoderName,
              },
            ),
        ),
        options,
      );
    },
  );
}
