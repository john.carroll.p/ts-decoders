import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { numberD } from './number';

/**
 * numberD()
 */

describe('numberD()', () => {
  it('init', () => {
    expect(numberD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = numberD();

      for (const item of [
        0,
        -14,
        100,
        4448928342948,
        0.123,
        -342.342342,
        3432432.4,
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [{}, null, undefined, 'str']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a number', {
            decoderName: 'numberD',
          }),
        ]);
      }
    });
  });
});
