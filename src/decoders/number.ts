import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

export type NumberDecoderOptions = SimpleDecoderOptions;

/**
 * Can be used to verify that an unknown value is a `number`.
 *
 * Related:
 * - integerD
 */
export function numberD(options: NumberDecoderOptions = {}): Decoder<number> {
  return new Decoder(
    (value): DecoderResult<number> =>
      Number.isFinite(value)
        ? ok(value as number)
        : err(value, 'invalid type', 'must be a number', 'numberD', options),
  );
}
