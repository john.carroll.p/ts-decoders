import { Decoder, DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  BOOLEAN_ASYNC_DEC,
  NUMBER_ASYNC_DEC,
  NUMBER_DEC,
  STRING_ASYNC_DEC,
  STRING_DEC,
} from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { objectD } from './object';

const UNDEFINABLE_BOOLEAN_D = new Decoder(input => {
  switch (typeof input) {
    case 'boolean':
    case 'undefined':
      return new DecoderSuccess(input);
  }

  return new DecoderError(
    input,
    'invalid-type',
    'must be a boolean or undefined',
  );
});

/**
 * objectD()
 */

describe('objectD()', () => {
  it('init', () => {
    expect(objectD({})).toBeDecoder();
  });

  describe('normal', () => {
    it('{STRING_DEC, NUMBER_DEC}', () => {
      const decoder = objectD({
        string: STRING_DEC,
        number: NUMBER_DEC,
      });

      for (const item of [
        { string: 'ste', number: 23 },
        { string: 'fead', number: -123.21 },
        { string: '', number: 100.32 },
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [
        { string: 'ste', number: 23, boolean: true, 21: 32 },
        {
          string: 'fead',
          number: -123.21,
          boolean: true,
          child: { string: '', number: 100.32, boolean: false },
        },
        { string: 'fdaf', number: 324, boolean: false, square: ['box'] },
      ]) {
        expect(decoder.decode(item)).not.toEqual(new DecoderSuccess(item));
        delete item[21];
        delete item.child;
        delete item.square;
        delete item.boolean;
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined, 'stead']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'objectD',
          }),
        ]);
      }

      const obj1 = { string: 'ste' };
      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'missing key',
          'missing required key ["number"]',
          {
            decoderName: 'objectD',
            child: new DecoderError(
              undefined,
              'invalid type',
              'must be a number',
            ),
          },
        ),
      ]);

      const obj2 = {
        string: 'fdaf',
        boolean: false,
        square: ['box'],
        number: '324',
      };
      assertDecodesToErrors(decoder, obj2, [
        new DecoderError(
          obj2,
          'invalid key value',
          'invalid value for key ["number"] > must be a number',
          {
            decoderName: 'objectD',
            location: 'number',
            key: 'number',
            child: new DecoderError(
              obj2.number,
              'invalid type',
              'must be a number',
            ),
          },
        ),
      ]);
    });

    it('{STRING_DEC,NUMBER_DEC},{allErrors: true}', () => {
      const decoder = objectD(
        {
          string: STRING_DEC,
          number: NUMBER_DEC,
        },
        {
          allErrors: true,
        },
      );

      for (const item of [
        { string: 'ste', number: 23 },
        { string: 'fead', number: -123.21 },
        { string: '', number: 100.32 },
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [
        { string: 'ste', number: 23, boolean: true, 21: 32 },
        {
          string: 'fead',
          number: -123.21,
          boolean: true,
          child: { string: '', number: 100.32, boolean: false },
        },
        { string: 'fdaf', number: 324, boolean: false, square: ['box'] },
      ]) {
        expect(decoder.decode(item)).not.toEqual(new DecoderSuccess(item));
        delete item[21];
        delete item.child;
        delete item.square;
        delete item.boolean;
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined, 'stead']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'objectD',
          }),
        ]);
      }

      const obj1 = { string: 'ste' };
      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'missing key',
          'missing required key ["number"]',
          {
            decoderName: 'objectD',
            child: new DecoderError(
              undefined,
              'invalid type',
              'must be a number',
            ),
          },
        ),
      ]);

      const obj2 = {
        string: {},
        square: ['box'],
        number: '324',
      };
      assertDecodesToErrors(decoder, obj2, [
        new DecoderError(
          obj2,
          'invalid key value',
          'invalid value for key ["number"] > must be a number',
          {
            decoderName: 'objectD',
            location: 'number',
            key: 'number',
            child: new DecoderError(
              obj2.number,
              'invalid type',
              'must be a number',
            ),
          },
        ),
        new DecoderError(
          obj2,
          'invalid key value',
          'invalid value for key ["string"] > must be a string',
          {
            decoderName: 'objectD',
            location: 'string',
            key: 'string',
            child: new DecoderError(
              obj2.string,
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it('{STRING_DEC, NUMBER_DEC}, {noExcessProperties: true}', () => {
      const decoder = objectD(
        {
          string: STRING_DEC,
          number: NUMBER_DEC,
        },
        {
          noExcessProperties: true,
        },
      );

      for (const item of [
        { string: 'ste', number: 23 },
        { string: 'fead', number: -123.21 },
        { string: '', number: 100.32 },
      ]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined, 'stead']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'objectD',
          }),
        ]);
      }

      const obj1 = { string: 'ste' };
      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'missing key',
          'missing required key ["number"]',
          {
            decoderName: 'objectD',
            child: new DecoderError(
              undefined,
              'invalid type',
              'must be a number',
            ),
          },
        ),
      ]);

      const obj2 = { string: 'ste', number: 23, boolean: true };
      assertDecodesToErrors(decoder, obj2, [
        new DecoderError(obj2, 'unknown key', 'unknown key ["boolean"]', {
          decoderName: 'objectD',
          location: 'boolean',
          key: 'boolean',
        }),
      ]);
    });

    it(
      '{STRING_DEC, NUMBER_DEC}, ' +
        '{noExcessProperties: true, allErrors: true}',
      () => {
        const decoder = objectD(
          {
            string: STRING_DEC,
            number: NUMBER_DEC,
          },
          {
            noExcessProperties: true,
            allErrors: true,
          },
        );

        for (const item of [
          { string: 'ste', number: 23 },
          { string: 'fead', number: -123.21 },
          { string: '', number: 100.32 },
        ]) {
          assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
        }

        for (const item of [0.123, true, null, undefined, 'stead']) {
          assertDecodesToErrors(decoder, item, [
            new DecoderError(
              item,
              'invalid type',
              'must be a non-null object',
              {
                decoderName: 'objectD',
              },
            ),
          ]);
        }

        const obj1 = { number: '23', boolean: true };
        assertDecodesToErrors(decoder, obj1, [
          new DecoderError(
            obj1,
            'invalid key value',
            'invalid value for key ["number"] > must be a number',
            {
              decoderName: 'objectD',
              location: 'number',
              key: 'number',
              child: new DecoderError(
                obj1.number,
                'invalid type',
                'must be a number',
              ),
            },
          ),
          new DecoderError(obj1, 'unknown key', 'unknown key ["boolean"]', {
            decoderName: 'objectD',
            location: 'boolean',
            key: 'boolean',
          }),
          new DecoderError(
            obj1,
            'missing key',
            'missing required key ["string"]',
            {
              decoderName: 'objectD',
              child: new DecoderError(
                undefined,
                'invalid type',
                'must be a string',
              ),
            },
          ),
        ]);
      },
    );

    it(
      '{STRING_DEC, objectD({NUMBER_DEC})}, ' +
        '{noExcessProperties: true, allErrors: true}',
      () => {
        const decoder = objectD(
          {
            string: STRING_DEC,
            object: objectD({ number: NUMBER_DEC }),
          },
          {
            noExcessProperties: true,
            allErrors: true,
          },
        );

        const obj1 = {
          string: 'ste',
          object: {
            deno: Symbol('lang'),
            number: 2342343,
          },
        };

        expect(decoder.decode(obj1)).not.toEqual(new DecoderSuccess(obj1));
        delete obj1.object.deno;
        assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

        const obj2 = {
          strig: 'ste',
          object: { number: 2342343 },
        };
        assertDecodesToErrors(decoder, obj2, [
          new DecoderError(obj2, 'unknown key', 'unknown key ["strig"]', {
            decoderName: 'objectD',
            location: 'strig',
            key: 'strig',
          }),
          new DecoderError(
            obj2,
            'missing key',
            'missing required key ["string"]',
            {
              decoderName: 'objectD',
              child: new DecoderError(
                undefined,
                'invalid type',
                'must be a string',
              ),
            },
          ),
        ]);

        const obj3 = {
          string: true,
          object: { number: '2342343' },
        };
        assertDecodesToErrors(decoder, obj3, [
          new DecoderError(
            obj3,
            'invalid key value',
            'invalid value for key ["string"] > must be a string',
            {
              decoderName: 'objectD',
              location: 'string',
              key: 'string',
              child: new DecoderError(
                obj3.string,
                'invalid type',
                'must be a string',
              ),
            },
          ),
          new DecoderError(
            obj3,
            'invalid key value',
            'invalid value for key ["object"] > invalid value for key ["number"] ' +
              '> must be a number',
            {
              decoderName: 'objectD',
              location: 'object.number',
              key: 'object',
              child: new DecoderError(
                obj3.object,
                'invalid key value',
                'invalid value for key ["number"] > must be a number',
                {
                  decoderName: 'objectD',
                  location: 'number',
                  key: 'number',
                  child: new DecoderError(
                    obj3.object.number,
                    'invalid type',
                    'must be a number',
                  ),
                },
              ),
            },
          ),
        ]);
      },
    );

    it(
      '{STRING_DEC, UNDEFINABLE_BOOLEAN_D}, ' +
        '{removeUndefinedProperties: true}',
      () => {
        const decoder = objectD(
          {
            string: STRING_DEC,
            boolean: UNDEFINABLE_BOOLEAN_D,
          },
          {
            removeUndefinedProperties: true,
          },
        );

        expect(decoder).toBeDecoder();

        const obj1 = {
          string: 'ste',
          boolean: true as boolean | undefined,
        };

        assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

        obj1.boolean = undefined;
        assertDecodesToSuccess(
          decoder,
          obj1,
          new DecoderSuccess({
            string: 'ste',
          }),
        );

        delete obj1.boolean;
        assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

        const obj2 = {
          string: 'ste' as string | undefined,
          boolean: false,
        };

        assertDecodesToSuccess(decoder, obj2, new DecoderSuccess(obj2));

        obj2.string = undefined;
        assertDecodesToErrors(decoder, obj2, [
          new DecoderError(
            obj2,
            'invalid key value',
            'invalid value for key ["string"] > must be a string',
            {
              decoderName: 'objectD',
              location: 'string',
              key: 'string',
              child: new DecoderError(
                obj2.string,
                'invalid type',
                'must be a string',
              ),
            },
          ),
        ]);
      },
    );

    it('{STRING_DEC, NUMBER_DEC}, {keepExcessProperties: true}', () => {
      type Input = {
        string: string;
        number: number;
        other?: Symbol;
      };

      const decoder = objectD<Input>(
        {
          string: STRING_DEC,
          number: NUMBER_DEC,
        },
        {
          keepExcessProperties: true,
        },
      );

      const items: Input[] = [
        { string: 'ste', number: 23, other: Symbol('other') },
        { string: 'fead', number: -123.21, other: Symbol('other') },
        { string: '', number: 100.32 },
      ];

      for (const item of items) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, null, undefined, 'stead']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a non-null object', {
            decoderName: 'objectD',
          }),
        ]);
      }

      const obj1 = { string: 'ste' };
      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'missing key',
          'missing required key ["number"]',
          {
            decoderName: 'objectD',
            child: new DecoderError(
              undefined,
              'invalid type',
              'must be a number',
            ),
          },
        ),
      ]);

      const obj2 = { string: 'ste', number: 23, boolean: true };
      assertDecodesToSuccess(decoder, obj2, new DecoderSuccess(obj2));
    });
  });

  describe('async', () => {
    it('{STRING_DEC, objectD({NUMBER_DEC})}', async () => {
      const decoder = objectD({
        string: STRING_DEC,
        object: objectD({ boolean: BOOLEAN_ASYNC_DEC }),
      });

      expect(decoder).toBeAsyncDecoder();

      const obj1 = {
        string: 'ste',
        object: {
          deno: Symbol('lang'),
          boolean: true,
        },
      };

      expect(decoder.decode(obj1)).toBeInstanceOf(Promise);
      expect(await decoder.decode(obj1)).not.toEqual(new DecoderSuccess(obj1));
      delete obj1.object.deno;
      await assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

      const obj2 = {
        strig: 'ste',
        object: { boolean: true },
      };
      await assertDecodesToErrors(decoder, obj2, [
        new DecoderError(
          obj2,
          'missing key',
          'missing required key ["string"]',
          {
            decoderName: 'objectD',
            child: new DecoderError(
              undefined,
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const obj3 = {
        string: true,
        object: { boolean: '2342343' },
      };
      await assertDecodesToErrors(decoder, obj3, [
        new DecoderError(
          obj3,
          'invalid key value',
          'invalid value for key ["string"] > must be a string',
          {
            decoderName: 'objectD',
            location: 'string',
            key: 'string',
            child: new DecoderError(
              obj3.string,
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);
    });

    it(
      '{STRING_DEC, objectD({NUMBER_DEC})}, ' +
        '{noExcessProperties: true, allErrors: true}',
      async () => {
        const decoder = objectD(
          {
            string: STRING_DEC,
            object: objectD({ number: NUMBER_ASYNC_DEC }),
          },
          {
            noExcessProperties: true,
            allErrors: true,
          },
        );

        expect(decoder).toBeAsyncDecoder();

        const obj1 = {
          string: 'ste',
          object: {
            deno: Symbol('lang'),
            number: 2342343,
          },
        };

        expect(decoder.decode(obj1)).toBeInstanceOf(Promise);
        expect(await decoder.decode(obj1)).not.toEqual(
          new DecoderSuccess(obj1),
        );
        delete obj1.object.deno;
        await assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

        const obj2 = {
          strig: 'ste',
          object: { number: 2342343 },
        };

        await assertDecodesToErrors(decoder, obj2, [
          new DecoderError(obj2, 'unknown key', 'unknown key ["strig"]', {
            decoderName: 'objectD',
            location: 'strig',
            key: 'strig',
          }),
          new DecoderError(
            obj2,
            'missing key',
            'missing required key ["string"]',
            {
              decoderName: 'objectD',
              child: new DecoderError(
                undefined,
                'invalid type',
                'must be a string',
              ),
            },
          ),
        ]);

        const obj3 = {
          string: true,
          object: { number: '2342343' },
        };

        await assertDecodesToErrors(decoder, obj3, [
          new DecoderError(
            obj3,
            'invalid key value',
            'invalid value for key ["string"] > must be a string',
            {
              decoderName: 'objectD',
              location: 'string',
              key: 'string',
              child: new DecoderError(
                obj3.string,
                'invalid type',
                'must be a string',
              ),
            },
          ),
          new DecoderError(
            obj3,
            'invalid key value',
            'invalid value for key ["object"] > ' +
              'invalid value for key ["number"] > must be a number',
            {
              decoderName: 'objectD',
              location: 'object.number',
              key: 'object',
              child: new DecoderError(
                obj3.object,
                'invalid key value',
                'invalid value for key ["number"] > must be a number',
                {
                  decoderName: 'objectD',
                  location: 'number',
                  key: 'number',
                  child: new DecoderError(
                    obj3.object.number,
                    'invalid type',
                    'must be a number',
                  ),
                },
              ),
            },
          ),
        ]);
      },
    );

    it(
      '{STRING_DEC, UNDEFINABLE_BOOLEAN_D}, ' +
        '{removeUndefinedProperties: true}',
      async () => {
        const decoder = objectD(
          {
            string: STRING_ASYNC_DEC,
            boolean: UNDEFINABLE_BOOLEAN_D,
          },
          {
            removeUndefinedProperties: true,
          },
        );

        expect(decoder).toBeAsyncDecoder();

        const obj1 = {
          string: 'ste',
          boolean: true as boolean | undefined,
        };

        await assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

        obj1.boolean = undefined;
        await assertDecodesToSuccess(
          decoder,
          obj1,
          new DecoderSuccess({
            string: 'ste',
          }),
        );

        delete obj1.boolean;
        await assertDecodesToSuccess(decoder, obj1, new DecoderSuccess(obj1));

        const obj2 = {
          string: 'ste' as string | undefined,
          boolean: false,
        };

        await assertDecodesToSuccess(decoder, obj2, new DecoderSuccess(obj2));

        obj2.string = undefined;
        await assertDecodesToErrors(decoder, obj2, [
          new DecoderError(
            obj2,
            'invalid key value',
            'invalid value for key ["string"] > must be a string',
            {
              decoderName: 'objectD',
              location: 'string',
              key: 'string',
              child: new DecoderError(
                obj2.string,
                'invalid type',
                'must be a string',
              ),
            },
          ),
        ]);
      },
    );
  });
});
