import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
} from 'ts-decoders';
import { errorLocation, ok } from './_util';
import { applyOptionsToDecoderErrors, ComposeDecoderOptions } from './util';

const decoderName = 'objectD';

function nonObjectError(
  input: unknown,
  options?: ObjectDecoderOptions,
): DecoderError[] {
  return applyOptionsToDecoderErrors(
    input,
    [
      new DecoderError(input, 'invalid type', 'must be a non-null object', {
        decoderName,
      }),
    ],
    options,
  );
}

function unknownKeyError(value: unknown, key: string): DecoderError {
  return new DecoderError(value, 'unknown key', `unknown key ["${key}"]`, {
    decoderName,
    location: errorLocation(key, ''),
    key,
  });
}

function fromChildError(
  child: DecoderError,
  value: object,
  key: string,
): DecoderError {
  let type: string;
  let message: string;
  let location: string | undefined;
  let errorKey: string | undefined = key;

  if (value.hasOwnProperty(key)) {
    location = errorLocation(key, child.location);
    type = 'invalid key value';
    message = `invalid value for key ["${key}"] > ${child.message}`;
  } else {
    type = 'missing key';
    message = `missing required key ["${key}"]`;
    location = undefined;
    errorKey = undefined;
  }

  return new DecoderError(value, type, message, {
    decoderName,
    child,
    location,
    key: errorKey,
  });
}

function checkInputKeys<T>(
  decoderObject: Partial<
    { [P in keyof T]: Decoder<T[P]> | AsyncDecoder<T[P]> }
  >,
  input: object,
  options: { allErrors?: boolean },
): DecoderError[] | undefined {
  const expectedkeys = Object.keys(decoderObject);
  const actualkeys = Object.keys(input);

  if (!options.allErrors) {
    const invalidKey = actualkeys.find(
      (key): boolean => !expectedkeys.includes(key),
    );

    if (invalidKey !== undefined) {
      return [unknownKeyError(input, invalidKey)];
    }
  } else {
    const invalidKeys = actualkeys.filter(
      (key): boolean => !expectedkeys.includes(key),
    );

    const errors = invalidKeys.map(
      (key): DecoderError => unknownKeyError(input, key),
    );

    if (errors.length > 0) return errors;
  }
}

export interface ObjectDecoderOptions extends ComposeDecoderOptions {
  noExcessProperties?: boolean;
  removeUndefinedProperties?: boolean;
  keepExcessProperties?: boolean;
}

/**
 * Accepts a `{[key: string]: Decoder<any> | AsyncDecoder<any>}` init object
 * argument and returns a new decoder that will verify that an input is a
 * non-null object, and that each element-key of the input is decoded by the
 * corresponding element-key of the init object. On `DecoderSuccess`, a new
 * object is returned which has element-values defined by the init object's
 * element-values. By default, any excess properties on the input object are
 * ignored (i.e. not included on the returned value).
 *
 * - _"object element" refers to a `key: value` pair of the object._
 * - _"element-value" refers to the `value` of this pair._
 * - _"element-key" refers to the `key` of this pair_
 *
 * Options:
 * - If you pass the `noExcessProperties: true` option, any excess properties
 *   on the input object will return a DecoderError.
 * - If you pass an `allErrors: true` option as well as any AsyncDecoders as
 *   arguments, then `objectD()` will create a new AsyncDecoder which decodes
 *   each key of the input object in parallel.
 * - If you pass the `removeUndefinedProperties: true` option, then after all
 *   other decoding of an input succeeds, any `undefined` properties are
 *   deleted from the result.
 *
 * Related:
 * - dictionaryD
 */

export function objectD<T>(
  decoderObject: Partial<{ [P in keyof T]: Decoder<T[P]> }>,
  options: ObjectDecoderOptions & { keepExcessProperties: true },
): Decoder<T>;
export function objectD<T>(
  decoderObject: Partial<
    { [P in keyof T]: Decoder<T[P]> | AsyncDecoder<T[P]> }
  >,
  options: ObjectDecoderOptions & { keepExcessProperties: true },
): AsyncDecoder<T>;
export function objectD<T>(
  decoderObject: { [P in keyof T]: Decoder<T[P]> },
  options?: ObjectDecoderOptions,
): Decoder<T>;
export function objectD<T>(
  decoderObject: { [P in keyof T]: Decoder<T[P]> | AsyncDecoder<T[P]> },
  options?: ObjectDecoderOptions,
): AsyncDecoder<T>;
export function objectD<T>(
  inputDecoderObject: Partial<
    { [P in keyof T]: Decoder<T[P]> | AsyncDecoder<T[P]> }
  >,
  options: ObjectDecoderOptions = {},
): Decoder<T> | AsyncDecoder<T> {
  const hasAsyncDecoder = Object.values(inputDecoderObject).some(
    (decoder): boolean => decoder instanceof AsyncDecoder,
  );

  options = {
    ...options,
    decoderName: options.decoderName || decoderName,
  };

  if (hasAsyncDecoder) {
    if (options.allErrors) {
      return new AsyncDecoder(
        async (input): Promise<DecoderResult<T>> => {
          if (typeof input !== 'object' || input === null) {
            return nonObjectError(input, options);
          }

          let decoderObject = inputDecoderObject;

          const allErrors: DecoderError[] = [];

          if (options.noExcessProperties) {
            const invalidKeys = checkInputKeys(decoderObject, input, options);

            if (invalidKeys) {
              allErrors.push(...invalidKeys);
            }
          }

          if (options.keepExcessProperties) {
            decoderObject = {
              ...Object.fromEntries(
                Object.entries(input).map(([k, v]) => [
                  k,
                  new Decoder(() => ok(v)),
                ]),
              ),
              ...decoderObject,
            };
          }

          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const entries: Array<[string, Decoder<any>]> = Object.entries(
            decoderObject,
          );

          let resolvedEntries = await Promise.all(
            entries.map(
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              async ([key, decoder]): Promise<[string, any] | undefined> => {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                const value = (input as { [key: string]: any })[key];

                const result = await decoder.decode(value);

                if (areDecoderErrors(result)) {
                  const errors = result.map(
                    (error): DecoderError => fromChildError(error, input, key),
                  );

                  allErrors.push(...errors);
                  return;
                }

                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                return [key, result.value] as [string, any];
              },
            ),
          );

          if (allErrors.length > 0) {
            return applyOptionsToDecoderErrors(input, allErrors, options);
          }

          if (options.removeUndefinedProperties) {
            resolvedEntries = (resolvedEntries as Array<[string, any]>).filter(
              ([, value]) => value !== undefined,
            );
          }

          return ok(
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            Object.fromEntries(resolvedEntries as Array<[string, any]>),
          );
        },
      );
    }

    return new AsyncDecoder(
      async (input): Promise<DecoderResult<T>> => {
        if (typeof input !== 'object' || input === null) {
          return nonObjectError(input, options);
        }

        let decoderObject = inputDecoderObject;

        if (options.noExcessProperties) {
          const invalidKeys = checkInputKeys(decoderObject, input, options);

          if (invalidKeys) {
            return applyOptionsToDecoderErrors(input, invalidKeys, options);
          }
        }

        if (options.keepExcessProperties) {
          decoderObject = {
            ...Object.fromEntries(
              Object.entries(input).map(([k, v]) => [
                k,
                new Decoder(() => ok(v)),
              ]),
            ),
            ...decoderObject,
          };
        }

        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const entries: Array<[string, Decoder<any>]> = Object.entries(
          decoderObject,
        );
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const resultObj: { [key: string]: any } = {};

        for (const [key, decoder] of entries) {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          const value = (input as { [key: string]: any })[key];

          const result = await decoder.decode(value);

          if (areDecoderErrors(result)) {
            const errors = result.map(
              (error): DecoderError => fromChildError(error, input, key),
            );

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          if (options.removeUndefinedProperties && result.value === undefined) {
            continue;
          }

          resultObj[key] = result.value;
        }

        return ok(resultObj) as DecoderResult<T>;
      },
    );
  }

  return new Decoder(
    (input): DecoderResult<T> => {
      if (typeof input !== 'object' || input === null) {
        return nonObjectError(input, options);
      }

      const allErrors: DecoderError[] = [];
      let decoderObject = inputDecoderObject;

      if (options.noExcessProperties) {
        const invalidKeys = checkInputKeys(decoderObject, input, options);

        if (invalidKeys) {
          if (!options.allErrors) {
            return applyOptionsToDecoderErrors(input, invalidKeys, options);
          }

          allErrors.push(...invalidKeys);
        }
      }

      if (options.keepExcessProperties) {
        decoderObject = {
          ...Object.fromEntries(
            Object.entries(input).map(([k, v]) => [
              k,
              new Decoder(() => ok(v)),
            ]),
          ),
          ...decoderObject,
        };
      }

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const entries: Array<[string, Decoder<any>]> = Object.entries(
        decoderObject,
      );
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const resultObj: { [key: string]: any } = {};

      for (const [key, decoder] of entries) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        const value = (input as { [key: string]: any })[key];

        const result = decoder.decode(value);

        if (areDecoderErrors(result)) {
          const errors = result.map(
            (error): DecoderError => fromChildError(error, input, key),
          );

          if (options.allErrors) {
            allErrors.push(...errors);
            continue;
          }

          return applyOptionsToDecoderErrors(input, errors, options);
        }

        if (options.removeUndefinedProperties && result.value === undefined) {
          continue;
        }

        resultObj[key] = result.value;
      }

      if (allErrors.length > 0) {
        return applyOptionsToDecoderErrors(input, allErrors, options);
      }

      return ok(resultObj) as DecoderResult<T>;
    },
  );
}
