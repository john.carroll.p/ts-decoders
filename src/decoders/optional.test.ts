import { DecoderError, DecoderSuccess } from 'ts-decoders';
import { STRING_ASYNC_DEC, STRING_DEC } from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { optionalD } from './optional';

/**
 * optionalD()
 */

describe('optionalD()', () => {
  it('init', () => {
    expect(optionalD(STRING_DEC)).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = optionalD(STRING_DEC);

      for (const item of ['2019-07-03', 'heLLooooo', null, undefined]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, {}, true]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid type',
            'must be a string OR must be null OR must be undefined',
            {
              decoderName: 'optionalD',
              child: new DecoderError(item, 'invalid type', 'must be a string'),
            },
          ),
        ]);
      }
    });
  });

  describe('async', () => {
    it('', async () => {
      const decoder = optionalD(STRING_ASYNC_DEC);

      expect(decoder).toBeAsyncDecoder();

      for (const item of ['2019-07-03', 'heLLooooo', null, undefined]) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, {}, true]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid type',
            'must be a string OR must be null OR must be undefined',
            {
              decoderName: 'optionalD',
              child: new DecoderError(item, 'invalid type', 'must be a string'),
            },
          ),
        ]);
      }
    });
  });
});
