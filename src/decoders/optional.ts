import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  isDecoderSuccess,
} from 'ts-decoders';
import { exactlyD } from './exactly';
import { applyOptionsToDecoderErrors, SimpleDecoderOptions } from './util';

const decoderName = 'optionalD';
const undefinedDecoder = exactlyD(undefined);
const nullDecoder = exactlyD(null);

export type OptionalDecoderOptions = SimpleDecoderOptions;

/**
 * Accepts a decoder and returns a new decoder which accepts
 * either the original decoder's value or `null` or `undefined`.
 *
 * Related:
 * - nullableD
 * - undefinableD
 */

export function optionalD<R, I>(
  decoder: Decoder<R, I>,
  options?: OptionalDecoderOptions,
): Decoder<R | null | undefined, I>;
export function optionalD<R, I>(
  decoder: AsyncDecoder<R, I>,
  options?: OptionalDecoderOptions,
): AsyncDecoder<R | null | undefined, I>;
export function optionalD<R, I>(
  decoder: Decoder<R, I> | AsyncDecoder<R, I>,
  options: OptionalDecoderOptions = {},
): Decoder<R | null | undefined, I> | AsyncDecoder<R | null | undefined, I> {
  if (decoder instanceof AsyncDecoder) {
    return new AsyncDecoder(
      async (input: I): Promise<DecoderResult<R | null | undefined>> => {
        let result: DecoderResult<
          R | null | undefined
        > = undefinedDecoder.decode(input);

        if (isDecoderSuccess(result)) return result;

        result = nullDecoder.decode(input);

        if (isDecoderSuccess(result)) return result;

        result = await decoder.decode(input);

        if (isDecoderSuccess(result)) return result;

        return applyOptionsToDecoderErrors(
          input,
          Array.from(result.values()).map(
            error =>
              new DecoderError(
                input,
                'invalid type',
                `${error.message} OR must be null OR must be undefined`,
                {
                  child: error,
                  decoderName,
                },
              ),
          ),
          options,
        );
      },
    );
  }

  return new Decoder(
    (input: I): DecoderResult<R | null | undefined> => {
      let result: DecoderResult<R | null | undefined> = undefinedDecoder.decode(
        input,
      );

      if (isDecoderSuccess(result)) return result;

      result = nullDecoder.decode(input);

      if (isDecoderSuccess(result)) return result;

      result = decoder.decode(input);

      if (isDecoderSuccess(result)) return result;

      return applyOptionsToDecoderErrors(
        input,
        Array.from(result.values()).map(
          (error): DecoderError =>
            new DecoderError(
              input,
              'invalid type',
              `${error.message} OR must be null OR must be undefined`,
              {
                child: error,
                decoderName,
              },
            ),
        ),
        options,
      );
    },
  );
}
