import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { predicateD } from './predicate';

/**
 * predicateD()
 */

describe('predicateD()', () => {
  it('init', () => {
    expect(predicateD((): boolean => true)).toBeDecoder();
    expect(
      predicateD((): boolean => false, { promise: true }),
    ).toBeAsyncDecoder();
    expect(
      predicateD((): Promise<boolean> => Promise.resolve(false), {
        promise: true,
      }),
    ).toBeAsyncDecoder();
  });

  describe('normal', () => {
    it('(any) => boolean', () => {
      const decoder = predicateD((v): boolean => typeof v === 'string');
      const msg = 'failed custom check';

      for (const item of ['0', 'two']) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, {}, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'failed check', msg, {
            decoderName: 'predicateD',
          }),
        ]);
      }
    });
  });

  describe('async', () => {
    it('(any) => boolean,{promise: true}', async () => {
      const decoder = predicateD((v): boolean => typeof v === 'string', {
        promise: true,
      });

      const msg = 'failed custom check';

      expect(decoder).toBeAsyncDecoder();

      for (const item of ['0', 'two']) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, true, {}, null, undefined]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'failed check', msg, {
            decoderName: 'predicateD',
          }),
        ]);
      }
    });
  });
});
