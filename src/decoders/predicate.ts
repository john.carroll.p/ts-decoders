import { AsyncDecoder, Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

const decoderName = 'predicateD';
const defaultMsg = 'failed custom check';

export interface PredicateDecoderOptions extends SimpleDecoderOptions {
  promise?: boolean;
}

/**
 * Accepts a predicate function argument and creates a decoder which
 * verifies that inputs pass the function check.
 *
 * **Async**:
 * To pass a predicate function which returns a promise resolving to a
 * boolean, pass the `promise: true` option to `predicateD()`.
 */

export function predicateD<I>(
  fn: (value: I) => boolean | Promise<boolean>,
  options: PredicateDecoderOptions & { promise: true },
): AsyncDecoder<I, I>;
export function predicateD<I, R extends I>(
  fn: (value: I) => value is R,
  options?: PredicateDecoderOptions,
): Decoder<R, I>;
export function predicateD<I>(
  fn: (value: I) => boolean,
  options?: PredicateDecoderOptions,
): Decoder<I, I>;
export function predicateD<I>(
  fn: (value: I) => boolean | Promise<boolean>,
  options: PredicateDecoderOptions = {},
): Decoder<I, I> | AsyncDecoder<I, I> {
  if (options.promise) {
    return new AsyncDecoder(
      async (input: I): Promise<DecoderResult<I>> =>
        (await fn(input))
          ? ok(input)
          : err(input, 'failed check', defaultMsg, decoderName, options),
    );
  }

  return new Decoder(
    (input: I): DecoderResult<I> =>
      fn(input)
        ? ok(input)
        : err(input, 'failed check', defaultMsg, decoderName, options),
  );
}
