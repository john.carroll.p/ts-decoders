import { DecoderError, DecoderSuccess } from 'ts-decoders';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { stringD } from './string';

/**
 * stringD()
 */

describe('stringD()', () => {
  it('init', () => {
    expect(stringD()).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = stringD();

      for (const item of ['0', '-14']) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0.123, -342.342342, {}, null, undefined]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be a string', {
            decoderName: 'stringD',
          }),
        ]);
      }
    });
  });
});
