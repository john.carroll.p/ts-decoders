import { Decoder, DecoderResult } from 'ts-decoders';
import { err, ok } from './_util';
import { SimpleDecoderOptions } from './util';

export type StringDecoderOptions = SimpleDecoderOptions;

/** Can be used to verify that an unknown value is a `string`. */
export function stringD(options: StringDecoderOptions = {}): Decoder<string> {
  return new Decoder(
    (value): DecoderResult<string> =>
      typeof value === 'string'
        ? ok(value)
        : err(value, 'invalid type', 'must be a string', 'stringD', options),
  );
}
