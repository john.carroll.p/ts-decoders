import { DecoderError, DecoderSuccess } from 'ts-decoders';
import { BOOLEAN_ASYNC_DEC, BOOLEAN_DEC, STRING_DEC } from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { tupleD } from './tuple';

/**
 * tupleD()
 */

describe('tupleD()', () => {
  it('init', () => {
    expect(tupleD([STRING_DEC])).toBeDecoder();
    expect(tupleD([STRING_DEC, STRING_DEC])).toBeDecoder();
  });

  describe('normal', () => {
    it('[STRING_DEC, BOOLEAN_DEC]', () => {
      const decoder = tupleD([STRING_DEC, BOOLEAN_DEC]);

      for (const item of [['string', true], ['', false]]) {
        // tupleD returns a new array so we can't use the `expected` option
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [{}, null, undefined, 1, true, 'happy']) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be an array', {
            decoderName: 'tupleD',
          }),
        ]);
      }

      const obj1 = [10, true];
      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'invalid element',
          'invalid element [0] > must be a string',
          {
            decoderName: 'tupleD',
            location: '[0]',
            key: 0,
            child: new DecoderError(
              obj1[0],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const obj2 = ['one', true, -2];
      assertDecodesToErrors(decoder, obj2, [
        new DecoderError(obj2, 'invalid length', 'array must have length 2', {
          decoderName: 'tupleD',
        }),
      ]);

      const obj3 = ['{}', {}];
      assertDecodesToErrors(decoder, obj3, [
        new DecoderError(
          obj3,
          'invalid element',
          'invalid element [1] > must be a boolean',
          {
            decoderName: 'tupleD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              obj3[1],
              'invalid type',
              'must be a boolean',
            ),
          },
        ),
      ]);
    });

    it('tupleD([STRING_DEC, BOOLEAN_DEC], {allErrors: true})', () => {
      const decoder = tupleD([STRING_DEC, BOOLEAN_DEC], {
        allErrors: true,
      });

      for (const item of [['string', true], ['', false]]) {
        // tupleD returns a new array so we can't use the `expected` option
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      const obj1 = ['{}', {}, true];
      assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'invalid element',
          'invalid element [1] > must be a boolean',
          {
            decoderName: 'tupleD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              obj1[1],
              'invalid type',
              'must be a boolean',
            ),
          },
        ),
        new DecoderError(obj1, 'invalid length', 'array must have length 2', {
          decoderName: 'tupleD',
        }),
      ]);
    });
  });

  describe('async', () => {
    it('[STRING_DEC, BOOLEAN_DEC]', async () => {
      const decoder = tupleD([STRING_DEC, BOOLEAN_ASYNC_DEC]);

      expect(decoder).toBeAsyncDecoder();

      for (const item of [['string', true], ['', false]]) {
        // tupleD returns a new array so we can't use the `expected` option
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [{}, null, undefined, 1, true, 'happy']) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(item, 'invalid type', 'must be an array', {
            decoderName: 'tupleD',
          }),
        ]);
      }

      const obj1 = [10, true];
      await assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'invalid element',
          'invalid element [0] > must be a string',
          {
            decoderName: 'tupleD',
            location: '[0]',
            key: 0,
            child: new DecoderError(
              obj1[0],
              'invalid type',
              'must be a string',
            ),
          },
        ),
      ]);

      const obj2 = ['one', true, -2];
      await assertDecodesToErrors(decoder, obj2, [
        new DecoderError(obj2, 'invalid length', 'array must have length 2', {
          decoderName: 'tupleD',
        }),
      ]);

      const obj3 = ['{}', {}];
      await assertDecodesToErrors(decoder, obj3, [
        new DecoderError(
          obj3,
          'invalid element',
          'invalid element [1] > must be a boolean',
          {
            decoderName: 'tupleD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              obj3[1],
              'invalid type',
              'must be a boolean',
            ),
          },
        ),
      ]);
    });

    it('[STRING_DEC, BOOLEAN_DEC], {allErrors: true}', async () => {
      const decoder = tupleD([STRING_DEC, BOOLEAN_ASYNC_DEC], {
        allErrors: true,
      });

      expect(decoder).toBeAsyncDecoder();

      for (const item of [['string', true], ['', false]]) {
        // tupleD returns a new array so we can't use the `expected` option
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      const obj1 = ['{}', {}, true];
      await assertDecodesToErrors(decoder, obj1, [
        new DecoderError(
          obj1,
          'invalid element',
          'invalid element [1] > must be a boolean',
          {
            decoderName: 'tupleD',
            location: '[1]',
            key: 1,
            child: new DecoderError(
              obj1[1],
              'invalid type',
              'must be a boolean',
            ),
          },
        ),
        new DecoderError(obj1, 'invalid length', 'array must have length 2', {
          decoderName: 'tupleD',
        }),
      ]);
    });
  });
});
