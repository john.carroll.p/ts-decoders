import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
} from 'ts-decoders';
import { err, errorLocation, ok } from './_util';
import { applyOptionsToDecoderErrors, ComposeDecoderOptions } from './util';

const decoderName = 'tupleD';

function nonArrayError(
  input: unknown,
  options: TupleDecoderOptions | undefined,
) {
  return err(input, 'invalid type', 'must be an array', decoderName, options);
}

function invalidLengthError(length: number, input: unknown) {
  return new DecoderError(
    input,
    'invalid length',
    `array must have length ${length}`,
    {
      decoderName,
    },
  );
}

function childError(
  child: DecoderError,
  value: unknown,
  key: number,
): DecoderError {
  const location = errorLocation(key, child.location);

  return new DecoderError(
    value,
    'invalid element',
    `invalid element [${key}] > ${child.message}`,
    {
      decoderName,
      child,
      location,
      key,
    },
  );
}

export type TupleDecoderOptions = ComposeDecoderOptions;

/**
 * Receives an array of decoders and creates a decoder which
 * can be used to verify that an input is:
 *
 * 1. An array of the same length as the decoder argument array.
 * 2. The first decoder argument will be used the process the
 *    first element of an input array.
 * 3. The second decoder argument will be used the process the
 *    second element of an input array.
 * 4. etc...
 *
 * Options:
 * - If you pass an `allErrors: true` option as well as any
 *   AsyncDecoders as arguments, then `tupleD()` will create a new
 *   AsyncDecoder which decodes each index of the input array in
 *   parallel.
 *
 * Related:
 * - arrayD
 */
export function tupleD<R extends [any, ...any[]]>(
  decoders: { [P in keyof R]: Decoder<R[P]> },
  options?: TupleDecoderOptions,
): Decoder<R>;

export function tupleD<R extends [any, ...any[]]>(
  decoders: { [P in keyof R]: Decoder<R[P]> | AsyncDecoder<R[P]> },
  options?: TupleDecoderOptions,
): AsyncDecoder<R>;

export function tupleD<R extends [any, ...any[]]>(
  decoders: { [P in keyof R]: Decoder<R[P]> | AsyncDecoder<R[P]> },
  options: TupleDecoderOptions = {},
): Decoder<R> | AsyncDecoder<R> {
  const hasAsyncDecoder = decoders.some(
    (decoder): boolean => decoder instanceof AsyncDecoder,
  );

  options = {
    ...options,
    decoderName: options.decoderName || decoderName,
  };

  if (hasAsyncDecoder) {
    if (options.allErrors) {
      return new AsyncDecoder(
        async (input): Promise<DecoderResult<R>> => {
          if (!Array.isArray(input)) {
            return nonArrayError(input, options);
          }

          let hasError = false;

          const results: Array<
            DecoderError[] | DecoderSuccess<any>
          > = await Promise.all(
            decoders.map(async (decoder, index) => {
              const result = await decoder.decode(input[index]);

              if (areDecoderErrors(result)) {
                hasError = true;
                return result.map(error => childError(error, input, index));
              }

              return result;
            }),
          );

          if (hasError || input.length !== decoders.length) {
            const errors = (results.filter(result =>
              Array.isArray(result),
            ) as DecoderError[][]).flat();

            if (input.length !== decoders.length) {
              errors.push(invalidLengthError(decoders.length, input));
            }

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          return ok(
            (results as Array<DecoderSuccess<unknown>>).map(
              (result): unknown => result.value,
            ),
          ) as DecoderSuccess<R>;
        },
      );
    }

    return new AsyncDecoder(
      async (input): Promise<DecoderResult<R>> => {
        if (!Array.isArray(input)) {
          return nonArrayError(input, options);
        } else if (input.length !== decoders.length) {
          return applyOptionsToDecoderErrors(
            input,
            invalidLengthError(decoders.length, input),
            options,
          );
        }

        const tuple: unknown[] = [];
        let index = -1;

        for (const decoder of decoders) {
          index++;

          const result = await decoder.decode(input[index]);

          if (areDecoderErrors(result)) {
            const errors = result.map(error => childError(error, input, index));

            return applyOptionsToDecoderErrors(input, errors, options);
          }

          tuple.push(result.value);
        }

        return ok(tuple) as DecoderSuccess<R>;
      },
    );
  }

  return new Decoder(
    (input): DecoderResult<R> => {
      if (!Array.isArray(input)) {
        return nonArrayError(input, options);
      }

      const tuple: unknown[] = [];
      const allErrors: DecoderError[] = [];
      let index = -1;

      if (input.length !== decoders.length) {
        if (!options.allErrors) {
          return applyOptionsToDecoderErrors(
            input,
            invalidLengthError(decoders.length, input),
            options,
          );
        }

        allErrors.push(invalidLengthError(decoders.length, input));
      }

      for (const decoder of decoders) {
        index++;

        const result = (decoder as Decoder<unknown, unknown>).decode(
          input[index],
        );

        if (areDecoderErrors(result)) {
          const errors = result.map(error => childError(error, input, index));

          if (options.allErrors) {
            allErrors.push(...errors);
            continue;
          }

          return applyOptionsToDecoderErrors(input, errors, options);
        }

        tuple.push(result.value);
      }

      if (allErrors.length > 0) {
        return applyOptionsToDecoderErrors(input, allErrors, options);
      }

      return ok(tuple) as DecoderSuccess<R>;
    },
  );
}
