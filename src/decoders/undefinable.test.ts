import { DecoderError, DecoderSuccess } from 'ts-decoders';
import { STRING_ASYNC_DEC, STRING_DEC } from '../testing';
import {
  assertDecodesToErrors,
  assertDecodesToSuccess,
} from '../testing/_util';
import { undefinableD } from './undefinable';

/**
 * undefinableD()
 */

describe('undefinableD()', () => {
  it('init', () => {
    expect(undefinableD(STRING_DEC)).toBeDecoder();
  });

  describe('normal', () => {
    it('', () => {
      const decoder = undefinableD(STRING_DEC);

      expect(decoder).toBeDecoder();

      for (const item of ['2019-07-03', 'heLLooooo', undefined]) {
        assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, {}, true, null]) {
        assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid type',
            'must be a string OR must be undefined',
            {
              decoderName: 'undefinableD',
              child: new DecoderError(item, 'invalid type', 'must be a string'),
            },
          ),
        ]);
      }
    });
  });

  describe('async', () => {
    it('', async () => {
      const decoder = undefinableD(STRING_ASYNC_DEC);

      expect(decoder).toBeAsyncDecoder();

      for (const item of ['2019-07-03', 'heLLooooo', undefined]) {
        await assertDecodesToSuccess(decoder, item, new DecoderSuccess(item));
      }

      for (const item of [0, {}, true, null]) {
        await assertDecodesToErrors(decoder, item, [
          new DecoderError(
            item,
            'invalid type',
            'must be a string OR must be undefined',
            {
              decoderName: 'undefinableD',
              child: new DecoderError(item, 'invalid type', 'must be a string'),
            },
          ),
        ]);
      }
    });
  });
});
