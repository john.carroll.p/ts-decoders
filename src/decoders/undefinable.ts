import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderResult,
  isDecoderSuccess,
} from 'ts-decoders';
import { exactlyD } from './exactly';
import { applyOptionsToDecoderErrors, SimpleDecoderOptions } from './util';

const decoderName = 'undefinableD';
const undefinedDecoder = exactlyD(undefined);
const errorsMapFn = <I>(input: I) => (error: DecoderError) =>
  new DecoderError(
    input,
    'invalid type',
    `${error.message} OR must be undefined`,
    {
      child: error,
      decoderName,
    },
  );

export type UndefinableDecoderOptions = SimpleDecoderOptions;

/**
 * Accepts a decoder and returns a new decoder which accepts
 * either the original decoder's value or `undefined`.
 *
 * Related:
 * - nullableD
 * - optionalD
 */
export function undefinableD<R, I>(
  decoder: Decoder<R, I>,
  options?: UndefinableDecoderOptions,
): Decoder<R | undefined, I>;
export function undefinableD<R, I>(
  decoder: AsyncDecoder<R, I>,
  options?: UndefinableDecoderOptions,
): AsyncDecoder<R | undefined, I>;
export function undefinableD<R, I>(
  decoder: Decoder<R, I> | AsyncDecoder<R, I>,
  options: UndefinableDecoderOptions = {},
): Decoder<R | undefined, I> | AsyncDecoder<R | undefined, I> {
  if (decoder instanceof AsyncDecoder) {
    return new AsyncDecoder(
      async (input: I): Promise<DecoderResult<R | undefined>> => {
        let result: DecoderResult<R | undefined> = undefinedDecoder.decode(
          input,
        );

        if (isDecoderSuccess(result)) return result;

        result = await decoder.decode(input);

        if (isDecoderSuccess(result)) return result;

        return applyOptionsToDecoderErrors(
          input,
          result.map(errorsMapFn(input)),
          options,
        );
      },
    );
  }

  return new Decoder(
    (input: I): DecoderResult<R | undefined> => {
      let result: DecoderResult<R | undefined> = undefinedDecoder.decode(input);

      if (isDecoderSuccess(result)) return result;

      result = decoder.decode(input);

      if (isDecoderSuccess(result)) return result;

      return applyOptionsToDecoderErrors(
        input,
        result.map(errorsMapFn(input)),
        options,
      );
    },
  );
}
