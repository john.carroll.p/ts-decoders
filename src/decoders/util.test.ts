import { DecoderError, DecoderResult, DecoderSuccess } from 'ts-decoders';
import {
  applyOptionsToDecoderErrors,
  DecoderErrorMsgArg,
  raceToDecoderSuccess,
} from './util';

function decoderErrors(): {
  nullValue: null;
  yesValue: 'yes';
  payloadValue: {};
  objValue: { payload: {} };
  errors: DecoderError[];
} {
  const nullValue = null;
  const yesValue = 'yes';
  const payloadValue = {};
  const objValue = { payload: payloadValue };

  return {
    nullValue,
    yesValue,
    payloadValue,
    objValue,
    errors: [
      new DecoderError(nullValue, 'null error', 'null error', {
        decoderName: 'nullDecoder',
      }),
      new DecoderError(yesValue, 'invalid type', 'must be a boolean', {
        decoderName: 'BOOLEAN_DEC',
      }),
      new DecoderError(
        objValue,
        'invalid key value',
        'invalid value for key ["payload"] > must be a string',
        {
          decoderName: 'objectDecoder',
          child: new DecoderError(
            payloadValue,
            'invalid type',
            'must be a string',
            {
              decoderName: 'STRING_DEC',
            },
          ),
          key: 'payload',
          location: 'payload',
        },
      ),
    ],
  };
}

/**
 * util
 */

describe('util', () => {
  describe('applyOptionsToDecoderErrors()', () => {
    let args: {
      nullValue: null;
      yesValue: 'yes';
      payloadValue: {};
      objValue: {
        payload: {};
      };
      errors: DecoderError[];
    };

    beforeEach(() => {
      args = decoderErrors();
    });

    it('{ allErrors: true }', () => {
      const errors = applyOptionsToDecoderErrors(null, args.errors, {
        allErrors: true,
      });

      expect(errors).toBeDecoderErrors([
        new DecoderError(args.nullValue, 'null error', 'null error', {
          decoderName: 'nullDecoder',
          location: '',
          key: undefined,
          child: undefined,
        }),
        new DecoderError(args.yesValue, 'invalid type', 'must be a boolean', {
          decoderName: 'BOOLEAN_DEC',
          location: '',
          key: undefined,
          child: undefined,
        }),
        new DecoderError(
          args.objValue,
          'invalid key value',
          'invalid value for key ["payload"] > must be a string',
          {
            decoderName: 'objectDecoder',
            location: 'payload',
            key: 'payload',
            child: new DecoderError(
              args.payloadValue,
              'invalid type',
              'must be a string',
              {
                decoderName: 'STRING_DEC',
                location: '',
                key: undefined,
                child: undefined,
              },
            ),
          },
        ),
      ]);
    });

    it('{ decoderName: "customDecoderName", allErrors: true }', () => {
      const errors = applyOptionsToDecoderErrors(null, args.errors, {
        decoderName: 'customDecoderName',
        allErrors: true,
      });

      expect(errors).toBeDecoderErrors([
        new DecoderError(args.nullValue, 'null error', 'null error', {
          decoderName: 'customDecoderName',
          location: '',
          key: undefined,
          child: undefined,
        }),
        new DecoderError(args.yesValue, 'invalid type', 'must be a boolean', {
          decoderName: 'customDecoderName',
          location: '',
          key: undefined,
          child: undefined,
        }),
        new DecoderError(
          args.objValue,
          'invalid key value',
          'invalid value for key ["payload"] > must be a string',
          {
            decoderName: 'customDecoderName',
            location: 'payload',
            key: 'payload',
            child: new DecoderError(
              args.payloadValue,
              'invalid type',
              'must be a string',
              {
                decoderName: 'STRING_DEC',
                location: '',
                key: undefined,
                child: undefined,
              },
            ),
          },
        ),
      ]);
    });

    it('{ errorMsg: "my custom error message", allErrors: true }', () => {
      const errors = applyOptionsToDecoderErrors(null, args.errors, {
        errorMsg: 'my custom error message',
        allErrors: true,
      });

      expect(errors).toBeDecoderErrors([
        new DecoderError(
          args.nullValue,
          'null error',
          'my custom error message',
          {
            decoderName: 'nullDecoder',
            location: '',
            key: undefined,
            child: undefined,
          },
        ),
        new DecoderError(
          args.yesValue,
          'invalid type',
          'my custom error message',
          {
            decoderName: 'BOOLEAN_DEC',
            location: '',
            key: undefined,
            child: undefined,
          },
        ),
        new DecoderError(
          args.objValue,
          'invalid key value',
          'my custom error message',
          {
            decoderName: 'objectDecoder',
            location: 'payload',
            key: 'payload',
            child: new DecoderError(
              args.payloadValue,
              'invalid type',
              'must be a string',
              {
                decoderName: 'STRING_DEC',
                location: '',
                key: undefined,
                child: undefined,
              },
            ),
          },
        ),
      ]);
    });

    it('{ allErrors: true, decoderName: string, errorMsg: string }', () => {
      const errors = applyOptionsToDecoderErrors(null, args.errors, {
        allErrors: true,
        decoderName: 'customDecoderName',
        errorMsg: 'my custom error message',
      });

      expect(errors).toBeDecoderErrors([
        new DecoderError(
          args.nullValue,
          'null error',
          'my custom error message',
          {
            decoderName: 'customDecoderName',
            location: '',
            key: undefined,
            child: undefined,
          },
        ),
        new DecoderError(
          args.yesValue,
          'invalid type',
          'my custom error message',
          {
            decoderName: 'customDecoderName',
            location: '',
            key: undefined,
            child: undefined,
          },
        ),
        new DecoderError(
          args.objValue,
          'invalid key value',
          'my custom error message',
          {
            decoderName: 'customDecoderName',
            location: 'payload',
            key: 'payload',
            child: new DecoderError(
              args.payloadValue,
              'invalid type',
              'must be a string',
              {
                decoderName: 'STRING_DEC',
                location: '',
                key: undefined,
                child: undefined,
              },
            ),
          },
        ),
      ]);
    });

    it('{ allErrors: true, decoderName: string, errorMsg: mutateFn }', () => {
      const mutateErrors: DecoderErrorMsgArg = (_, errors) => {
        errors.forEach((error): void => {
          error.location = 'funky location';
        });

        return errors;
      };

      const errors = applyOptionsToDecoderErrors(null, args.errors, {
        allErrors: true,
        decoderName: 'customDecoderName',
        errorMsg: mutateErrors,
      });

      expect(errors).toBeDecoderErrors([
        new DecoderError(args.nullValue, 'null error', 'null error', {
          decoderName: 'customDecoderName',
          location: 'funky location',
          key: undefined,
          child: undefined,
        }),
        new DecoderError(args.yesValue, 'invalid type', 'must be a boolean', {
          decoderName: 'customDecoderName',
          location: 'funky location',
          key: undefined,
          child: undefined,
        }),
        new DecoderError(
          args.objValue,
          'invalid key value',
          'invalid value for key ["payload"] > must be a string',
          {
            decoderName: 'customDecoderName',
            location: 'funky location',
            key: 'payload',
            child: new DecoderError(
              args.payloadValue,
              'invalid type',
              'must be a string',
              {
                decoderName: 'STRING_DEC',
                location: '',
                key: undefined,
                child: undefined,
              },
            ),
          },
        ),
      ]);
    });

    it('{ allErrors: true, decoderName: string, errorMsg: replaceFn }', () => {
      const replaceErrors: DecoderErrorMsgArg = (_, errors) => {
        return errors.map(
          error =>
            new DecoderError(error.input, error.type, error.message, {
              decoderName: 'myDecoder',
              child: error.child,
            }),
        );
      };

      const errors = applyOptionsToDecoderErrors(null, args.errors, {
        allErrors: true,
        decoderName: 'customDecoderName',
        errorMsg: replaceErrors,
      });

      expect(errors).toBeDecoderErrors([
        new DecoderError(args.nullValue, 'null error', 'null error', {
          decoderName: 'myDecoder',
          child: undefined,
        }),
        new DecoderError(args.yesValue, 'invalid type', 'must be a boolean', {
          decoderName: 'myDecoder',
          child: undefined,
        }),
        new DecoderError(
          args.objValue,
          'invalid key value',
          'invalid value for key ["payload"] > must be a string',
          {
            decoderName: 'myDecoder',
            child: new DecoderError(
              args.payloadValue,
              'invalid type',
              'must be a string',
              {
                decoderName: 'STRING_DEC',
                location: '',
                key: undefined,
                child: undefined,
              },
            ),
          },
        ),
      ]);
    });
  });

  describe('raceToDecoderSuccess()', () => {
    const decoderSuccess = (ms: number) =>
      new Promise<DecoderSuccess<number>>(res => {
        setTimeout(() => res(new DecoderSuccess(ms)), ms);
      });

    const decoderError = (ms: number) =>
      new Promise<DecoderError[]>(res => {
        setTimeout(() => res([new DecoderError(ms, `${ms}`, `${ms}`)]), ms);
      });

    // Tmportant to test these serially rather than in parallel.
    // tests seem to not work if done in parallel.
    it('when success', async () => {
      async function testSuccessRace(
        promises: Array<Promise<DecoderResult<unknown>>>,
        time: number,
        success: DecoderSuccess<unknown>,
      ) {
        // const start = performance.now();

        await expect(raceToDecoderSuccess(promises)).toBeAsyncDecoderSuccess(
          success,
        );

        // const end = performance.now();

        // expect(end - start).toBeLessThanOrEqual(time);
      }

      await testSuccessRace(
        [decoderSuccess(20), decoderError(10), decoderError(30)],
        29,
        new DecoderSuccess(20),
      );

      await testSuccessRace(
        [decoderSuccess(20), decoderError(10), decoderError(30)],
        29,
        new DecoderSuccess(20),
      );

      await testSuccessRace(
        [decoderError(10), decoderSuccess(20), decoderError(30)],
        29,
        new DecoderSuccess(20),
      );

      await testSuccessRace(
        [decoderError(10), decoderError(30), decoderSuccess(20)],
        29,
        new DecoderSuccess(20),
      );

      await testSuccessRace(
        [decoderError(10), decoderSuccess(20), decoderSuccess(30)],
        29,
        new DecoderSuccess(20),
      );

      await testSuccessRace(
        [decoderSuccess(10), decoderSuccess(20), decoderSuccess(30)],
        19,
        new DecoderSuccess(10),
      );

      await testSuccessRace(
        [decoderSuccess(20), decoderSuccess(20), decoderSuccess(20)],
        29,
        new DecoderSuccess(20),
      );
    });

    // Tmportant to test these serially rather than in parallel.
    // tests seem to not work if done in parallel.
    it('when fail', async () => {
      async function testErrorRace(
        promises: Array<Promise<DecoderResult<unknown>>>,
        time: number,
        errors: DecoderError[],
      ): Promise<void> {
        // const start = performance.now();

        await expect(raceToDecoderSuccess(promises)).toBeAsyncDecoderErrors(
          errors,
        );

        // const end = performance.now();

        // expect(end - start).toBeLessThanOrEqual(time);
      }

      await testErrorRace(
        [decoderError(10), decoderError(20), decoderError(30)],
        39,
        [
          new DecoderError(10, '10', '10'),
          new DecoderError(20, '20', '20'),
          new DecoderError(30, '30', '30'),
        ],
      );

      await testErrorRace(
        [
          decoderError(10),
          decoderError(20),
          decoderError(30),
          decoderError(40),
          decoderError(40),
        ],
        49,
        [
          new DecoderError(10, '10', '10'),
          new DecoderError(20, '20', '20'),
          new DecoderError(30, '30', '30'),
          new DecoderError(40, '40', '40'),
          new DecoderError(40, '40', '40'),
        ],
      );
    });
  });
});
