import {
  areDecoderErrors,
  DecoderError,
  DecoderResult,
  DecoderSuccess,
} from 'ts-decoders';

export interface SimpleDecoderOptions {
  decoderName?: string;
  data?: any;
  errorMsg?: DecoderErrorMsgArg;
}

export interface ComposeDecoderOptions extends SimpleDecoderOptions {
  allErrors?: boolean;
}

export type DecoderErrorMsgArg =
  | string
  | ((input: unknown, errors: DecoderError[]) => DecoderError | DecoderError[]);

export function applyOptionsToDecoderErrors(
  input: any,
  errors: DecoderError | DecoderError[],
  options: ComposeDecoderOptions | undefined,
): DecoderError[] {
  if (errors instanceof DecoderError) {
    errors = [errors];
  }

  if (!options) return errors;

  if (options.decoderName !== undefined && options.data !== undefined) {
    Array.from(errors.values()).forEach(error => {
      error.data = options.data;
      error.decoderName = options.decoderName!;
    });
  } else if (options.decoderName) {
    Array.from(errors.values()).forEach(error => {
      error.decoderName = options.decoderName!;
    });
  } else if (options.data !== undefined) {
    Array.from(errors.values()).forEach(error => {
      error.data = options.data;
    });
  }

  if (typeof options.errorMsg === 'function') {
    errors = options.errorMsg(input, errors);

    if (errors instanceof DecoderError) errors = [errors];
  } else if (options.errorMsg) {
    Array.from(errors.values()).forEach(error => {
      error.message = options.errorMsg as string;
      return error;
    });
  }

  if (errors.length === 0) {
    throw new Error(
      'Provided DecoderError function must return ' +
        'an array of DecoderErrors with length greater than 0.',
    );
  }

  return errors;
}

async function _raceToDecoderSuccess<T>(
  promises: Array<Promise<DecoderResult<T>>>,
  errors: DecoderError[] = [],
): Promise<DecoderSuccess<T> | DecoderError[]> {
  if (promises.length === 0) return errors;

  const indexedPromises = promises.map(
    (promise, index): Promise<[number, DecoderResult<T>]> =>
      promise.then(value => [index, value]),
  );

  const res = await Promise.race(indexedPromises);

  if (areDecoderErrors(res[1])) {
    promises.splice(res[0], 1);

    errors.push(...res[1]);

    return _raceToDecoderSuccess(promises, errors);
  }

  return res[1];
}

/**
 * Resolves the provided DecoderResults, returning the first
 * DecoderSuccess or all the DecoderErrors.
 */
export function raceToDecoderSuccess<T>(
  promises: Array<Promise<DecoderResult<T>>>,
): Promise<DecoderSuccess<T> | DecoderError[]> {
  return _raceToDecoderSuccess(promises);
}
