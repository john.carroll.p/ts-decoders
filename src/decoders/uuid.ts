import { matchD } from './match';
import { SimpleDecoderOptions } from './util';

export type UUIDDecoderOptions = SimpleDecoderOptions;

/** Can be used to verify that an unknown value is a uuid v4 `string`. */
export function uuidD(options: UUIDDecoderOptions = {}) {
  return matchD(
    /[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/i,
    {
      data: options.data,
      decoderName: options.decoderName || 'uuidD',
      errorMsg: options.errorMsg || 'must be a v4 UUID',
    },
  );
}
