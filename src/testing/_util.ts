import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderSuccess,
} from 'ts-decoders';

export function assertDecodesToSuccess(
  decoder: Decoder<any>,
  input: any,
  success: DecoderSuccess<any>,
): void;
export function assertDecodesToSuccess(
  decoder: AsyncDecoder<any>,
  input: any,
  success: DecoderSuccess<any>,
): Promise<void>;
export function assertDecodesToSuccess(
  decoder: Decoder<any> | AsyncDecoder<any>,
  input: any,
  success: DecoderSuccess<any>,
): void | Promise<void> {
  if (decoder instanceof AsyncDecoder) {
    return expect([decoder, input]).toAsyncDecodeSuccessfully(success);
  }

  return expect([decoder, input]).toDecodeSuccessfully(success);
}

export function assertDecodesToErrors(
  decoder: Decoder<any>,
  input: any,
  errors: DecoderError | DecoderError[],
): void;
export function assertDecodesToErrors(
  decoder: AsyncDecoder<any>,
  input: any,
  errors: DecoderError | DecoderError[],
): Promise<void>;
export function assertDecodesToErrors(
  decoder: Decoder<any> | AsyncDecoder<any>,
  input: any,
  errors: DecoderError | DecoderError[],
): void | Promise<void> {
  if (decoder instanceof AsyncDecoder) {
    return expect([decoder, input]).toAsyncDecodeWithErrors(errors);
  }

  return expect([decoder, input]).toDecodeWithErrors(errors);
}
