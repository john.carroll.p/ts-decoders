import {
  areDecoderErrors,
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderSuccess,
} from 'ts-decoders';

/**
 * Test Decoders
 */

/** Simple boolean Decoder for use in tests */
export const BOOLEAN_DEC = new Decoder(value =>
  typeof value === 'boolean'
    ? new DecoderSuccess(value)
    : new DecoderError(value, 'invalid type', 'must be a boolean'),
);

/** Simple string Decoder for use in tests */
export const STRING_DEC = new Decoder(value =>
  typeof value === 'string'
    ? new DecoderSuccess(value)
    : new DecoderError(value, 'invalid type', 'must be a string'),
);

/** Simple number Decoder for use in tests */
export const NUMBER_DEC = new Decoder(value =>
  typeof value === 'number'
    ? new DecoderSuccess(value)
    : new DecoderError(value, 'invalid type', 'must be a number'),
);

/** Simple `any` Decoder for use in tests */
export const ANY_DEC = new Decoder(value => new DecoderSuccess(value));

export interface NestedArray {
  [key: number]: NestedArray;
}

/** Recursive "nested arrays" Decoder for use in tests */
export const RECURSIVE_DEC: Decoder<NestedArray[]> = new Decoder(input => {
  if (!Array.isArray(input)) {
    return new DecoderError(input, 'invalid type', 'must be an array');
  }

  let index = -1;
  const array: NestedArray[] = [];

  for (const element of input) {
    index++;
    const result = RECURSIVE_DEC.decode(element);

    if (areDecoderErrors(result)) {
      return result.map(
        error =>
          new DecoderError(
            element,
            'invalid element',
            `invalid element [${index}] > ${error.message}`,
            {
              location: `[${index}]${error.location}`,
              key: index,
              child: error,
            },
          ),
      );
    }

    array.push(result.value);
  }

  return new DecoderSuccess(array);
});

/**
 * Test AsyncDecoders
 */

/** Simple boolean AsyncDecoder for use in tests */
export const BOOLEAN_ASYNC_DEC = new AsyncDecoder(async value =>
  typeof value === 'boolean'
    ? new DecoderSuccess(value)
    : new DecoderError(value, 'invalid type', 'must be a boolean'),
);

/** Simple number AsyncDecoder for use in tests */
export const STRING_ASYNC_DEC = new AsyncDecoder(async value =>
  typeof value === 'string'
    ? new DecoderSuccess(value)
    : new DecoderError(value, 'invalid type', 'must be a string'),
);

/** Simple number AsyncDecoder for use in tests */
export const NUMBER_ASYNC_DEC = new AsyncDecoder(async value =>
  typeof value === 'number'
    ? new DecoderSuccess(value)
    : new DecoderError(value, 'invalid type', 'must be a number'),
);

/** Simple number AsyncDecoder for use in tests */
export const ANY_ASYNC_DEC = new AsyncDecoder(
  async value => new DecoderSuccess(value),
);

/** Recursive "nested array" AsyncDecoder for use in tests */
export const RECURSIVE_ASYNC_DEC: AsyncDecoder<NestedArray> = new AsyncDecoder(
  async input => {
    if (!Array.isArray(input)) {
      return new DecoderError(input, 'invalid type', 'must be an array');
    }

    let index = -1;
    const array: NestedArray[] = [];

    for (const element of input) {
      index++;
      const result = await RECURSIVE_ASYNC_DEC.decode(element);

      if (areDecoderErrors(result)) {
        return result.map(
          error =>
            new DecoderError(
              element,
              'invalid element',
              `invalid element [${index}] > ${error.message}`,
              {
                location: `[${index}]${error.location}`,
                key: index,
                child: error,
              },
            ),
        );
      }

      array.push(result.value);
    }

    return new DecoderSuccess(array);
  },
);
