import {
  AsyncDecoder,
  Decoder,
  DecoderError,
  DecoderSuccess,
} from 'ts-decoders';

declare global {
  namespace jest {
    interface Matchers<R> {
      /**
       * Use .toBeDecoder when checking if a value is a Decoder.
       */
      toBeDecoder(): any;

      /**
       * Use .toBeAsyncDecoder when checking if a value is a
       * AsyncDecoder.
       */
      toBeAsyncDecoder(): any;

      /**
       * Use .toBeDecoderSuccess when checking if a value is a
       * DecoderSuccess.
       */
      toBeDecoderSuccess(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toBeAsyncDecoderSuccess when checking if a value is a
       * Promise<DecoderSuccess>.
       */
      toBeAsyncDecoderSuccess(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toBeDecoderErrors when checking if a value is an array
       * of DecoderError.
       */
      toBeDecoderErrors(expected: DecoderError | DecoderError[]): any;

      /**
       * Use .toBeAsyncDecoderErrors when checking if a value is an
       * array of Promise<DecoderError>.
       */
      toBeAsyncDecoderErrors(expected: DecoderError | DecoderError[]): any;

      /**
       * Use .toDecodeWithErrors when checking if a decoder decodes
       * a value to a DecoderSuccess.
       */
      toDecodeSuccessfully(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toAsyncDecodeSuccessfully when checking if a decoder
       * decodes a value to a Promise<DecoderSuccess>.
       */
      toAsyncDecodeSuccessfully(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toDecodeWithErrors when checking if a decoder decodes
       * a value to an array of DecoderError.
       */
      toDecodeWithErrors(expected: DecoderError | DecoderError[]): any;

      /**
       * Use .toAsyncDecodeWithErrors when checking if a decoder
       * decodes a value to a Promise<DecoderError[]>.
       */
      toAsyncDecodeWithErrors(expected: DecoderError | DecoderError[]): any;
    }
  }
}

function printValue(value: unknown): string | Promise<unknown> {
  return value instanceof Promise ? value : JSON.stringify(value);
}

/**
 * Make an assertion that `actual` is a Decoder.
 *
 * If not then throw.
 */
expect.extend({
  toBeDecoder(received) {
    return {
      message: () => `"${printValue(received)}" expected to be a Decoder`,
      pass: received instanceof Decoder,
    };
  },
});

/**
 * Make an assertion that `actual` is a AsyncDecoder.
 *
 * If not then throw.
 */
expect.extend({
  toBeAsyncDecoder(received) {
    return {
      message: () => `"${printValue(received)}" expected to be a AsyncDecoder`,
      pass: received instanceof AsyncDecoder,
    };
  },
});

/**
 * Make an assertion that `actual` is a DecoderSuccess.
 *
 * If not then throw.
 */
expect.extend({
  toBeDecoderSuccess(received, expected: DecoderSuccess<unknown>) {
    const pass = !(received instanceof Promise);

    if (!pass) {
      return {
        message: () => `"${printValue(received)}" expected to not be a promise`,
        pass: false,
      };
    }

    expect(received).toEqual(expected);

    return {
      message: `"${printValue(received)}" expected to equal ${printValue(
        expected,
      )}`,
      pass: true,
    };
  },
});

/**
 * Make an assertion that `actual` is a DecoderSuccess
 * with matching `value` to `expected`.
 *
 * If not then throw.
 */
expect.extend({
  async toBeAsyncDecoderSuccess(received, expected: DecoderSuccess<unknown>) {
    const pass = received instanceof Promise;

    if (!pass) {
      return {
        message: () => `"${printValue(received)}" expected to be a promise`,
        pass: false,
      };
    }

    const value = await received;

    expect(value).toEqual(expected);

    return {
      message: `"${printValue(value)}" expected to equal ${printValue(
        expected,
      )}`,
      pass: true,
    };
  },
});

/**
 * Make an assertion that `actual` is an array of DecoderError
 * with matching properties and size to `expected`. Also asserts that
 * `actual` is NOT an instanceof `Promise`.
 *
 * If not then throw.
 */
expect.extend({
  toBeDecoderErrors(received, expected: DecoderError | DecoderError[]) {
    if (received instanceof Promise) {
      return {
        message: () => `"${printValue(received)}" expected to not be a promise`,
        pass: false,
      };
    }

    if (
      !Array.isArray(received) ||
      received.some(val => !(val instanceof DecoderError))
    ) {
      return {
        message: () =>
          `${printValue(received)} expected to be an array of DecoderError`,
        pass: false,
      };
    }

    const normExpected =
      expected instanceof DecoderError ? [expected] : expected;

    if (received.length !== normExpected.length) {
      return {
        message: () =>
          `${printValue(received)} expected to have length ${
            normExpected.length
          } but had length ${received.length}`,
        pass: false,
      };
    }

    normExpected.forEach(value => {
      expect(received).toContainEqual(value);
    });

    return {
      message: `${printValue(received)} expected to contain items`,
      pass: true,
    };
  },
});

/**
 * Similar to `assertDecoderErrors`, except also asserts that
 * `actual` is an instanceof `Promise`.
 */
expect.extend({
  async toBeAsyncDecoderErrors(
    received,
    expected: DecoderError | DecoderError[],
  ) {
    if (!(received instanceof Promise)) {
      return {
        message: () => `"${printValue(received)}" expected to be a promise`,
        pass: false,
      };
    }

    const value = await received;

    expect(value).toBeDecoderErrors(expected);

    return {
      message: `${printValue(value)} expected to be an array of decoder errors`,
      pass: true,
    };
  },
});

/**
 * Make an assertion that `decoder.decode(value)` resolves to a
 * DecoderSuccess with matching `value` to `expected`.
 */
expect.extend({
  toDecodeSuccessfully(
    [decoder, value]: [Decoder<any>, any],
    expected: DecoderSuccess<unknown>,
  ) {
    expect(decoder.decode(value)).toBeDecoderSuccess(expected);

    return {
      message: `${printValue(value)} expected to decode successfully`,
      pass: true,
    };
  },
});

expect.extend({
  async toAsyncDecodeSuccessfully(
    [decoder, value]: [Decoder<any> | AsyncDecoder<any>, any | Promise<any>],
    expected: DecoderSuccess<unknown>,
  ) {
    if (!(decoder instanceof AsyncDecoder || value instanceof Promise)) {
      return {
        message: () =>
          `${decoder &&
            decoder.constructor.name} expected to be a AsyncDecoder`,
        pass: false,
      };
    }

    await expect(decoder.decode(value)).toBeAsyncDecoderSuccess(expected);

    return {
      message: `${printValue(value)} expected to async decode successfully`,
      pass: true,
    };
  },
});

/**
 * Make an assertion that `decoder.decode(value)` resolves to an
 * array of DecoderError with matching properties and size to
 * `expected`.
 *
 * If not then throw.
 */
expect.extend({
  toDecodeWithErrors(
    [decoder, value]: [Decoder<any>, any],
    expected: DecoderError | DecoderError[],
  ) {
    expect(decoder.decode(value)).toBeDecoderErrors(expected);

    return {
      message: `${printValue(value)} expected to decode with errors`,
      pass: true,
    };
  },
});

expect.extend({
  async toAsyncDecodeWithErrors(
    [decoder, value]: [Decoder<any> | AsyncDecoder<any>, any | Promise<any>],
    expected: DecoderError | DecoderError[],
  ) {
    if (!(decoder instanceof AsyncDecoder || value instanceof Promise)) {
      return {
        message: () =>
          `${decoder.constructor.name} expected to be a AsyncDecoder`,
        pass: false,
      };
    }

    await expect(decoder.decode(value)).toBeAsyncDecoderErrors(expected);

    return {
      message: `${printValue(value)} expected to async decode with errors`,
      pass: true,
    };
  },
});
