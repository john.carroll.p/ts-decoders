import { build } from './utils';

console.log('building src');

build({
  declarationPath: './build/es2015/src',
  rollupCommands: [
    'yarn rollup -c rollup-esm.config.js',
    'yarn rollup -c rollup-umd.config.js',
  ],
}).catch(e => {
  console.error(e);
  process.exit(1);
});
