import { DecoderError, DecoderSuccess } from '../packages/core/decoder_result';

declare global {
  namespace jest {
    interface Matchers<R> {
      /**
       * Use .toBeDecoder when checking if a value is a Decoder.
       */
      toBeDecoder(): any;

      /**
       * Use .toBeAsyncDecoder when checking if a value is a
       * AsyncDecoder.
       */
      toBeAsyncDecoder(): any;

      /**
       * Use .toBeDecoderSuccess when checking if a value is a
       * DecoderSuccess.
       */
      toBeDecoderSuccess(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toBeAsyncDecoderSuccess when checking if a value is a
       * Promise<DecoderSuccess>.
       */
      toBeAsyncDecoderSuccess(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toBeDecoderErrors when checking if a value is an array
       * of DecoderError.
       */
      toBeDecoderErrors(expect: DecoderError[]): any;

      /**
       * Use .toBeAsyncDecoderErrors when checking if a value is an
       * array of Promise<DecoderError>.
       */
      toBeAsyncDecoderErrors(expect: DecoderError[]): any;

      /**
       * Use .toDecodeWithErrors when checking if a decoder decodes
       * a value to a DecoderSuccess.
       */
      toDecodeSuccessfully(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toAsyncDecodeSuccessfully when checking if a decoder
       * decodes a value to a Promise<DecoderSuccess>.
       */
      toAsyncDecodeSuccessfully(expect: DecoderSuccess<unknown>): any;

      /**
       * Use .toDecodeWithErrors when checking if a decoder decodes
       * a value to an array of DecoderError.
       */
      toDecodeWithErrors(expect: DecoderError[]): any;

      /**
       * Use .toAsyncDecodeWithErrors when checking if a decoder
       * decodes a value to a Promise<DecoderError[]>.
       */
      toAsyncDecodeWithErrors(expect: DecoderError[]): any;
    }
  }
}
